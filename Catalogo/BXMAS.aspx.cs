﻿//Garcia Sanchez Alberto Abel

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{

    private String usr = "AGTESYFT";
    private int id = 8;
    private int agente = 1911;
    private String path = @"C:\Users\ultron\Desktop\";
    private List<String> errores;
    private List<Marca> marcas;
    private List<SubMarca> submarcas;
    private List<modelo> Modelos;



    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        errores = new List<string>();
        marcas = new List<Marca>();
        submarcas = new List<SubMarca>();
        Modelos = new List<modelo>();

    String error = "";

        try
        {
            BX.CatalogosAutosWsClient Bx = new BX.CatalogosAutosWsClient();
            if (getMarcas(Bx) && this.errores.Count == 0)
            {
                if (getSubMarca(Bx) && this.errores.Count == 0 && this.marcas.Count != 0)
                {
                    if (getModelo(Bx) && this.errores.Count == 0 && this.submarcas.Count != 0)
                    {
                        if (getVersion(Bx) && this.errores.Count == 0)
                        {
                            Almacenar();
                        }
                    }
                }
            }
            else
                errores.Add("hubo un error al obtener las marcas");
        }
        catch(Exception exc)
        {
            errores.Add(exc.Message+"\n");
        }

        if (this.errores.Count != 0)
        {
            foreach (String str in this.errores)
                error += str + "<br>";
        }
        else
            error = "Se a completado con exito";

        Label1.Text = error;
    }

    private Boolean getMarcas(BX.CatalogosAutosWsClient bx)
    {
        String salida = "";

        try
        {
            BX.fabricanteDto[] producto = bx.recuperarCatalogoFabricante("AUTOS", 1445 , 1 , this.agente, this.id , 1);
            foreach(BX.fabricanteDto fab in producto)
            {
                salida += fab.descMarca + "|" + fab.idMarca + "<br>";
                this.marcas.Add(new Marca(fab.descMarca,fab.idMarca));
            }
            CrearTxt(salida,"BXMAS_MARCAS");
            return true;
        }
        catch(Exception exc)
        {
            this.errores.Add(exc.Message+"\n");
            return false;
        }
    }



    private Boolean getSubMarca(BX.CatalogosAutosWsClient bx)
    {
        String salida = "";

        try
        {
            foreach(Marca marca in this.marcas)
            {
                BX.subMarcaDto[] subMarcas = bx.recuperarCatalogoSubmarca(marca.getId());
                foreach(BX.subMarcaDto sub in subMarcas)
                {
                    salida += sub.descSubMarca + "|" + sub.idSubMarca + "|" + marca.getId()+ "<br>";
                    this.submarcas.Add(new SubMarca(sub.descSubMarca,marca.getId(),sub.idSubMarca));
                }
            }
               CrearTxt(salida, "BXMAS_submarcas");
            return true;
        }
        catch (Exception exc)
        {
            this.errores.Add(exc.Message+" |getSubMarcas| \n");
            return false;
        }
    }

    private Boolean getModelo(BX.CatalogosAutosWsClient bx)
    {
        String salida = "";
       
            foreach(SubMarca sub in this.submarcas)
            {
                try
                {
                    BX.modeloDto[] modelos = bx.recuperarCatalogoModelo("AUTOS", 484, 1445, 1, 1, sub.getId(), sub.getDesc());
                    foreach (BX.modeloDto modelo in modelos)
                    {
                        salida += sub.getId() + "|" + sub.getDesc() + "|" + modelo.idModelo + "|" + modelo.idVersionCarga +  "<br>";
                        Modelos.Add(new modelo(sub.getId(),sub.getDesc(),modelo.idModelo,modelo.idVersionCarga));
                    }
                }
                catch (Exception exc)
                {
                    if (exc.Message != "No se encontro ningun modelo.")
                    {
                        this.errores.Add(exc.Message + " |getModelo| \n");
                        return false;
                    }
                }
            }

        CrearTxt(salida, "BXMAS_Modelos");
        return true;
    }

    private Boolean getVersion(BX.CatalogosAutosWsClient bx)
    {

        String salida = "";

        foreach(modelo mod in this.Modelos)
        {
            try
            {
                BX.versionDto[] versiones = bx.recuperarCatalogoVersion(mod.id,mod.descSub,mod.año,mod.id_carga,484);
                foreach(BX.versionDto dato in versiones)
                {
                    salida +=mod.id + "|" + mod.año + "|" + dato.descSubMarca+ "|" + dato.capacidadTonelada + "|" + dato.cveAire + "|" + dato.cveBolsaAire + "|" + dato.cveCilindros +"|"+ dato.cveElevadores+ "|" + dato.cveFrenos + "|" + dato.cveInyeccion + "|" + dato.cveQuemacocos +"|"+ dato.cveSonido + "|" + dato.cveTransmision + "|" + dato.cveUnidadMedida + "|" + dato.cveVestidura+ "|"+dato.descAlarma + "|" + dato.descVersion + "|" + dato.idEstilo +"|"+ dato.idTipoVehiculo + "|" + dato.idVersionRegistro + "|" + dato.numAsientos + "|" + dato.numPuertas + "|" + dato.sumaAseguradaValorComercial + "|" + dato.sumaAseguradaValorNuevo + "<br>";
                }
            }
            catch(Exception exc)
            {
                if(exc.Message != "No se encontro ninguna vesion para la submarca "+mod.descSub+".")
                {
                    this.errores.Add(exc.Message + " |getVersion| \n");
                    return false;
                }
            }   
        }

        CrearTxt(salida,"BXMAS_Versiones");
        return true;
    }

    private void CrearTxt(String salida, String name)
    {
        //inicia el intento de crear el archivo
        try
        {
            //genera el archivo dentro del escritorio con el nombre y la cadena dada
            FileStream file = File.Create(this.path + name + ".txt");
            Byte[] info = new UTF8Encoding(true).GetBytes(salida);
            file.Write(info, 0, info.Length);
            file.Close();
        }
        catch (Exception excep)
        {
            this.errores.Add(excep.Message + "|" + excep.InnerException + "|" + excep.StackTrace);
        }
    }


    private Boolean Almacenar()
    {
        try
        {
            //se crea la conexion a la base de datos , los parametros tienen que cambiarse
            using (var conn = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=BXMASCatalogos;user id = servici1; password = Master@011"))
            {
                //se accede y se ejecuta a el stop procedure y se le asigna un timeout de 0 para que no interrumpa la conexion
                using (var command = new SqlCommand("[SP.BXMASCatalogos]", conn) { CommandType = CommandType.StoredProcedure })
                {
                    command.CommandTimeout = 0;
                    conn.Open();
                    command.ExecuteNonQuery();
                    conn.Close();


                    return true;
                }
            }
        }
        catch (Exception excep)
        {
            this.errores.Add(excep.Message + " |Almacenar| \n");
            return false;
        }
    }

}

public class Marca
{
   private String marca { get; set; }
   private int id { get; set; }

    public int getId()
    {
        return this.id;
    }

    public Marca(String marca , int id)
    {
        this.marca = marca;
        this.id = id;
    }
}


public class SubMarca
{
    private String submarca { get; set; }
    private int id_marca { get; set; }
    private int id_submarca { get; set; }
    public int getId()
    {
        return this.id_marca;
    }
    public int getSubId()
    {
        return this.id_submarca;
    }

    public String getDesc()
    {
        return this.submarca;
    }

    public SubMarca(String submarca, int id,int subId)
    {
        this.submarca = submarca;
        this.id_marca = id;
        this.id_submarca = subId;
    }
}


public class modelo
{
    public int id { get; set; }
    public string descSub { get; set; }

    public int año { get; set; }

    public int id_carga { get; set; }

    public modelo(int id, string desc,int año,int id_carga)
    {
        this.id = id;
        this.descSub = desc;
        this.año = año;
        this.id_carga = id_carga;
    }


}


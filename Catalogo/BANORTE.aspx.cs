//Garcia Sanchez Alberto Abel

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.ServiceModel.Security;
using System.Text;
using System.Threading;

public partial class _Default : System.Web.UI.Page
{

    private static Boolean process = false;
    private static Boolean proccesComplet = false;
    private static String content = "";
    private static Semaphore _pool;
    private delegate void SafeCallDelegate(string text);

    private String user_name = "WS3B502";
    private String numero_oficina = "3B5";
    private String password = "11220";
    List<Marca> marcasLista;
    List<TipoV> tipoV;
    private List<string> errores;
    private List<string> nopass;
    private List<string> trackMS;
    List<Marca> tipoVehiculos;
    List<Marca> usoPasajeros;
    private static DateTime time;
    private static String[] subramos = { "01", "02", "03", "04", "05", "06", "08", "09", "20", "21", "22", "23" };


    protected void Page_Load(object sender, EventArgs e)
    {

    }


    public void Timer1_Tick(object sender , EventArgs e)
    {
        if (process)
        {
            lbl1.Text = "Tiempo transcurrido: "+(DateTime.Now-time).ToString();
            TextBox1.Text = content;
        }

        if(proccesComplet && content.Contains("Terminado"))
        {
            Timer1.Enabled = false;
            process = false;
            Button1.Enabled = true;
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
         marcasLista = new List<Marca>();
         tipoV = new List<TipoV>();
         errores = new List<string>();
         nopass = new List<string>();
         tipoVehiculos = new List<Marca>();
         usoPasajeros = new List<Marca>();

    Timer1.Enabled = true;
        proccesComplet = false;
        content = "";
        time = DateTime.Now;
        Button1.Enabled = false;
        if (_pool != null)
            _pool.Dispose();

        Thread work = new Thread(() => doThings());
        work.Start();
    }
    //----------------------------------------------Funcion principal----------------------------------------
    public void doThings()
    {
         try
        {
            process = true;
            BanorteWS.WSCatalogoBGClient banorte = new BanorteWS.WSCatalogoBGClient();
            banorte.ClientCredentials.UserName.UserName = user_name + "|" + numero_oficina;
            banorte.ClientCredentials.UserName.Password = password;
            banorte.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;

            if (errores.Count==0 && GetMarcas(banorte))
            {
                if (errores.Count == 0 && obtenerNoPasajeros(banorte))
                {
                    if (errores.Count == 0 && getUsos(banorte))
                    {
                        if (errores.Count == 0 && tipoVehiculo(banorte))
                        {
                            if (errores.Count == 0 && obtenerModelos(banorte))
                            {
                                if (errores.Count == 0 && getDescripcionVehiculos(banorte))
                                {
                                    Almacenar();
                                }
                            }
                        }
                    }
                }
            }

            proccesComplet = true;
            content = "Terminado"+ System.Environment.NewLine + content; 
        }
        catch (Exception exc)
        {
            errores.Add(exc.Message + " |Main Function|");
        }
    }


    //---------------------------------------Funciones principales--------------------------------------

    private Boolean GetMarcas(BanorteWS.WSCatalogoBGClient banorte)
    {
        string salida = "";

        try
        {
            _pool = new Semaphore(1, 50);
            Thread[] threads = new Thread[50];

            for (int i = 1981; i < DateTime.Now.Year + 2;)
            {
                for(int j = 0; j<50;)
                {
                    if (i< DateTime.Now.Year + 2)
                    {
                       
                        threads[j] = new Thread(() => getMR(banorte, i.ToString()));
                        threads[j].Start();
                        Thread.Sleep(60);
                    }
                     j++; i++;
                }
                foreach (Thread th in threads)
                {
                    if (th != null && th.IsAlive)
                        th.Join();
                }
            }
            
            foreach (Marca marc in marcasLista)
            {
                salida += marc.marca + "|" + marc.anios + "|" + marc.clv + "<br>";
            }
            CrearTxt(salida, "banorte_marcas");
            _pool.Dispose();
            threads = null;
            return true;
        }
        catch (Exception exc)
        {
            errores.Add(exc.Message + " |GetMarcas|");
            return false;
        }

    }


    public Boolean obtenerNoPasajeros(BanorteWS.WSCatalogoBGClient banorte)
    {
        try
        {
            _pool = new Semaphore(1, 200);
            Thread[] threads = new Thread[200];

            String salida = "";
            trackMS = new List<string>();

            foreach (String str in subramos)
            {
                for (int j = 0; j< marcasLista.Count;)
                {
                   for(int i = 0; i<200;i++)
                    {
                        if((j<marcasLista.Count && !trackMS.Contains(marcasLista[j].marca)))
                        {
                            threads[i] = new Thread(() => getNP(banorte, marcasLista[j], str));
                            threads[i].Start();
                            trackMS.Add(marcasLista[j].marca);
                            Thread.Sleep(60);
                        }
                        j++;
                    }
                    foreach (Thread th in threads)
                    {
                        if (th != null && th.IsAlive)
                            th.Join();
                    }
                }
                Thread.Sleep(50);
                trackMS.Clear();

            }
            foreach (string str in nopass)
            {
                salida += str;
            }
            CrearTxt(salida, "banorte_no_pasajeros");
            _pool.Dispose();
            threads = null;
            return true;
        }
        catch (Exception exc)
        {
            errores.Add(exc.Message + " |GetMarcas|");
            return false;
        }
    }

    private Boolean getUsos(BanorteWS.WSCatalogoBGClient banorte)
    {
        try
        {
            nopass.Clear();
            string salida = "";
            trackMS.Clear();
            _pool = new Semaphore(1,200);
            Thread[] threads = new Thread[200];

            foreach (String str in subramos)
            {
                for (int j = 0; j < marcasLista.Count;)
                {
                    for (int i = 0; i < 200; i++)
                    {
                        if ((j < marcasLista.Count && !trackMS.Contains(marcasLista[j].marca)))
                        {
                            threads[i] = new Thread(() => getUS(banorte, marcasLista[j], str));
                            threads[i].Start();
                            trackMS.Add(marcasLista[j].marca);
                            Thread.Sleep(60);
                        }
                        j++;

                    }
                    foreach (Thread th in threads)
                    {
                        if (th != null && th.IsAlive)
                            th.Join();
                    }
                }
                Thread.Sleep(50);
                trackMS.Clear();
            }
            foreach (string str in nopass)
            {
                salida += str;
            }
            CrearTxt(salida, "Banorte_usos");
            _pool.Dispose();
            threads = null;
            return true;
        }
        catch(Exception exc)
        {
            errores.Add(exc.Message + " |GetMarcas|");
            return false;
        }
    }

    public Boolean tipoVehiculo(BanorteWS.WSCatalogoBGClient banorte)
    {
        try
        {
            nopass.Clear();
            string salida = "";
            trackMS.Clear();
            _pool = new Semaphore(1, 200);
            Thread[] threads = new Thread[200];

            foreach (String str in subramos)
            {
                for (int j = 0; j < marcasLista.Count;)
                {
                    for (int i = 0; i < 200; i++)
                    {
                        if ((j < marcasLista.Count && !trackMS.Contains(marcasLista[j].marca)))
                        {
                            threads[i] = new Thread(() => getTV(banorte, marcasLista[j], str));
                            threads[i].Start();
                            trackMS.Add(marcasLista[j].marca);
                            Thread.Sleep(60);
                        }
                        j++;

                    }
                    foreach (Thread th in threads)
                    {
                        if (th != null && th.IsAlive)
                            th.Join();
                    }
                }
                Thread.Sleep(50);
                trackMS.Clear();
            }
            foreach (string str in nopass)
            {
                salida += str;
            }
            _pool.Dispose();
            threads = null;
            return true;
        }
        catch(Exception exc)
        {
            errores.Add(exc.Message + " |GetMarcas|");
            return false;
        }
    }

    public Boolean obtenerModelos(BanorteWS.WSCatalogoBGClient banorte)
    {
        try
        {
            nopass.Clear();
            String salida = "";
            _pool = new Semaphore(1, 800);
            Thread[] threads = new Thread[800];

            for (int i = 0; i < tipoV.Count;)
            {
                for(int j = 0; j<800;j++)
                {
                    if(i<tipoV.Count)
                    {
                        threads[j] = new Thread(() => getMD(banorte, tipoV[i]));
                        threads[j].Start();
                        Thread.Sleep(60);
                    }
                    i++;
                }
                foreach (Thread th in threads)
                {
                    if (th != null && th.IsAlive)
                        th.Join();
                }
            }
            foreach (string str in nopass)
            {
                salida += str;
            }

            CrearTxt(salida, "banorte_modelos");
            _pool.Dispose();
            threads = null;
            return true;
        }
        catch(Exception exc)
        {
            errores.Add(exc.Message + " |GetMarcas|");
            return false;
        }
    }

    public Boolean getDescripcionVehiculos(BanorteWS.WSCatalogoBGClient banorte)
    {
        try
        {
            nopass.Clear();
            string salida = "";
            trackMS.Clear();
            _pool = new Semaphore(1, 800);
            Thread[] threads = new Thread[800];

            for(int i=0; i<this.tipoV.Count;)
            {
                for(int j = 0;j<800;)
                {
                    if (i < tipoV.Count)
                    {
                        threads[j] = new Thread(() => getDv(banorte, this.tipoV[i]));
                        threads[j].Start();
                        Thread.Sleep(60);
                    }
                    i++;
                    j++;
                }
                
                foreach (Thread th in threads)
                {
                    if (th != null && th.IsAlive)
                        th.Join();
                }
            }

            foreach (string str in nopass)
            {
                salida += str;
            }

            CrearTxt(salida, "banorte_descripciones");
            _pool.Dispose();
            threads = null;
            return true;
        }
        catch(Exception exc)
        {
            errores.Add(exc.Message + " |GetMarcas|");
            return false;
        }
       

    }

    //------------------------------------Funciones asincronas--------------------------------------

    public void getMR(BanorteWS.WSCatalogoBGClient banorte , string a�os)
    {
        _pool.WaitOne();
        try
        {
            BanorteWS.Marca[] arrmarcas = null;
            BanorteWS.TokenResult rest = banorte.ObtenerMarcas("01", a�os, BanorteWS.DatoFlotilla.NO, ref arrmarcas);
            if (rest.Ok)
            {
                if (arrmarcas != null)
                {

                    foreach (BanorteWS.Marca marcas in arrmarcas)
                    {
                        Marca marca = new Marca(a�os, marcas.Descripcion, marcas.Codigo);
                        marcasLista.Add(marca);
                    }
                }
            }
            content = "Terminando de obtener las marcas del a�o " + a�os + System.Environment.NewLine + content;
        }
        catch (Exception exc)
        {
            errores.Add(exc.Message + " |GetMarcas|");
            content = "ah habido un error obteniendo las marcas del a�o " + a�os + System.Environment.NewLine + content;
        }
        finally
        {
            _pool.Release();
        }

    }

    public void getNP(BanorteWS.WSCatalogoBGClient banorte , Marca marca , string str)
    {
        _pool.WaitOne();

        try
        {
            BanorteWS.Pasajero[] pasajeros = null;
            banorte.ObtenerNumeroPasajeros(marca.clv, str, ref pasajeros);

            if (pasajeros!=null)
            {
                foreach (BanorteWS.Pasajero pasajero in pasajeros)
                {
                    nopass.Add(pasajero.Cantidad + "|" + marca.clv + "|" + str + "<br>");
                }
                content = "Terminando de obtener el numero de pasajeros de la marca " + marca.marca + " del subramo " + str + System.Environment.NewLine + content;
            }
        }
        catch(Exception exc)
        {
            errores.Add(exc.Message + " |GetMarcas|");
            content = "ah habido un error obteniendo el numero de pasajeros de la marca " + marca.marca + " del subramo " + str + System.Environment.NewLine + content;
        }
        finally
        {
            _pool.Release();
        }

    }

    public void getUS(BanorteWS.WSCatalogoBGClient banorte, Marca marca, string str)
    {
        _pool.WaitOne();
        try
        {
            BanorteWS.Uso[] uso = null;
            banorte.ObtenerUsos(str, marca.clv, BanorteWS.DatoFlotilla.NO, ref uso);
            if(uso!=null)
            {
                foreach (BanorteWS.Uso usos in uso)
                {
                    nopass.Add(marca.clv + "|" + usos.Codigo + "|" + usos.Descripcion + "<br>");
                }

                content = "Terminando de obtener el usode la marca " + marca.marca + " del subramo " + str + System.Environment.NewLine + content;
            }
        }
        catch (Exception exc)
        {
            errores.Add(exc.Message + " |GetMarcas|");
            content = "ah habido un error obteniendo el uso de la marca " + marca.marca + " del subramo " + str + System.Environment.NewLine + content;

        }
        finally
        {
            _pool.Release();
        }
    }

    public void getTV(BanorteWS.WSCatalogoBGClient banorte, Marca marca, string str)
    {
        _pool.WaitOne();
        try
        {
            BanorteWS.TipoVehiculo[] tipoVehiculo = null;
            banorte.ObtenerTipoVehiculo(str, BanorteWS.DatoFlotilla.NO, marca.clv, ref tipoVehiculo);

            if(tipoVehiculo!=null)
            {
                foreach (BanorteWS.TipoVehiculo vehiculo in tipoVehiculo)
                {
                    TipoV marc = new TipoV("", vehiculo.Descripcion, vehiculo.Codigo, marca.clv, str);
                    this.tipoV.Add(marc);
                }
                content = "terminando de obtener el tipo de vehiculo " + marca.marca + " del subramo " + str + System.Environment.NewLine + content;
            }
        }
        catch (Exception excep)
        {
            errores.Add(excep.Message + " |GetMarcas|");
            content = "ah habido un error obteniendo el tipo de vehiculo " + marca.marca + " del subramo " + str + System.Environment.NewLine + content;
        }
        finally
        {
            _pool.Release();
        }
    }


    public void getMD(BanorteWS.WSCatalogoBGClient banorte , TipoV tipo)
    {
        _pool.WaitOne();
        try
        {
            BanorteWS.Modelo[] modelos = null;
            banorte.ObtenerModelos(tipo.sub_ramo, tipo.cod_marca, tipo.cod_vehi, BanorteWS.DatoFlotilla.NO, ref modelos);
            if(modelos!=null)
            {
                foreach (BanorteWS.Modelo marcas in modelos)
                {
                    tipo.setA�o(marcas.Descripcion);
                    nopass.Add(marcas.Descripcion + "|" + tipo.desc_vehi + "|" + tipo.cod_vehi + "|" + tipo.cod_marca + "|" + tipo.sub_ramo + "<br>");
                }
                content = "Terminado de obtener el modelo de la sumbarca " + tipo.desc_vehi + " con el subramo " + tipo.sub_ramo + System.Environment.NewLine + content;

            }

        }
        catch(Exception excep)
        {
            errores.Add(excep.Message + " |GetMarcas|");
            content = "ah habido un error obteniendo el modelo de la sumbarca " + tipo.desc_vehi + " con el subramo " + tipo.sub_ramo + System.Environment.NewLine + content;
        }
        finally
        {
            _pool.Release();

        }

    }

    public void getDv(BanorteWS.WSCatalogoBGClient banorte, TipoV tipo)
    {
        _pool.WaitOne();

        try
        {
            foreach(string a�o in tipo.a�o)
            {
                BanorteWS.Descripcion[] descripcion1 = null;
                banorte.ObtenerDescripcion(tipo.sub_ramo, a�o, BanorteWS.DatoFlotilla.NO, tipo.cod_vehi, tipo.cod_marca, ref descripcion1);
                if (descripcion1 != null)
                {
                    foreach (BanorteWS.Descripcion descripcion in descripcion1)
                    {
                         nopass.Add(tipo.cod_marca + "|" + tipo.cod_vehi + "|" + a�o + "|" + descripcion.Codigo + "|" + descripcion.Descripciones + "<br>");
                    }
                    content = "Se obtuvo la descripcion de la sumbarca " + tipo.desc_vehi + " con el subramo " + tipo.sub_ramo +" del a�o " + a�o + System.Environment.NewLine + content;
                }
            }

        }
        catch(Exception exc)
        {
            errores.Add(exc.Message + " |GetMarcas|");
            content = "ah habido un error obteniendo la descripcion de la sumbarca " + tipo.desc_vehi + " con el subramo " + tipo.sub_ramo + System.Environment.NewLine + content;
        }
        finally
        {
            _pool.Release();
        }


    }

    //public void obtenerSubramos(BanorteWS.WSCatalogoBGClient banorte)
    //{
    //    BanorteWS.Subramo[] sub = null;
    //    String salida = "";
    //    banorte.ObtenerSubramo(BanorteWS.DatoFlotilla.SI,ref sub);
    //    foreach(BanorteWS.Subramo subr in sub)
    //    {
    //        salida += subr.Codigo + subr.CodigoSubramo + subr.Descripcion;
    //    }
    //    CrearTxt(salida,"subramos");
    //}







    public String CrearTxt(String salida, String name)
    {
        try
        {
            FileStream file = File.Create(@"C:\Users\ultron\Desktop\" + name + ".txt");
            Byte[] info = new UTF8Encoding(true).GetBytes(salida);
            file.Write(info, 0, info.Length);
            file.Close();
            return "ok";
        }
        catch (Exception exc)
        {
            return exc.Message;
        }
    }

    private String Almacenar()
    {
        try
        {
            //se crea la conexion a la base de datos , los parametros tienen que cambiarse
            using (var conn = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=BanorteCatalogos;user id = servici1; password = Master@011"))
            {
                //se accede y se ejecuta a el stop procedure y se le asigna un timeout de 0 para que no interrumpa la conexion
                using (var command = new SqlCommand("SP.BanorteCatalogos", conn) { CommandType = CommandType.StoredProcedure })
                {
                    command.CommandTimeout = 0;
                    conn.Open();
                    command.ExecuteNonQuery();
                    conn.Close();


                    return "ok";
                }
            }
        }
        catch (Exception excep)
        {
            return excep.Message;
        }
    }


}

public class MarcasBan
{
    public String marca { get; set; }
    public String anios { get; set; }
    public String clv { get; set; }
    public MarcasBan(String anios, String marca, String clv)
    {
        this.anios = anios;
        this.marca = marca;
        this.clv = clv;
    }

}
public class Marca
{

    public String marca { get; set; }
    public String anios { get; set; }
    public String clv { get; set; }

    public String clv_marca;

    public string sub_ramo;
    public Marca(String anios, String marca , String clv)
    {  
        this.anios = anios;
        this.marca = marca;
        this.clv = clv;
    }

    public Marca(String anios, String marca, String clv , String clv_marca , String sub_ramo)
    {
        this.anios = anios;
        this.marca = marca;
        this.clv = clv;
        this.clv_marca = clv_marca;
        this.sub_ramo = sub_ramo;
    }
   
    public void ModificarAnio(String anio)
    {
        this.anios += "," + anio;
    }

}
public class TipoV
{
    public List<string> a�o = null;
    public string desc_vehi { set; get; }
    public string cod_vehi { set; get; }
    public string cod_marca { get; set; }
    public string sub_ramo { get; set; }

    public TipoV (String anios, String marca, String clv, String clv_marca, String sub_ramo)
    {
        this.a�o = new List<string>();
        this.desc_vehi = marca;
        this.cod_vehi = clv;
        this.cod_marca = clv_marca;
        this.sub_ramo = sub_ramo;
    }

    public void setA�o(string str)
    {
        a�o.Add(str);
    }

}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;

public partial class Default2 : System.Web.UI.Page 
{

    //--------------------------------Atributos de la clase------------------------------------------------------------------
    private delegate void SafeCallDelegate(string text);
    private String path = @"C:\Users\ultron\Desktop\";
    private List<String> errores;
    private List<string> track;
    protected static string content;
    protected static bool inProcess = false;
    protected static bool processComplete = false;
    protected static string processCompleteMsg = "Finished Processing All Records.";
    private static Semaphore _pool;
    private static int i;
    private static int j;

    private string url = "https://www.segurosafirme.com.mx/MidasWeb/CotizacionAutoIndividualService";
    private string token = "a6131309-d28e-4004-b804-73797279e408";


    private TimeSpan tiempoTrans;
    private static DateTime time;

    private List<MArcasAfi> marcas;



    protected void Page_Load(object sender, EventArgs e)
    {

    }

    //---------------------------------------------------- Trigger del boton --------------------------------------------------

    protected void Button1_Click(object sender, EventArgs e)
    {
        errores = new List<String>();
        track = new List<string>();
        marcas = new List<MArcasAfi>();

        Timer1.Enabled = true;
        processComplete = false;
        content = "";
        Button1.Enabled = false;
        i = 0;j = 0;
        time = DateTime.Now;
        if(_pool!=null)
            _pool.Dispose();

        

        Thread thread = new Thread(() => doThings());
        thread.Start();

    }

    //---------------------------------------------------Funcion principal--------------------------------------------------------

    public void doThings()
    {
        try
        {
            inProcess = true;
            if (errores.Count==0 && getMarcas())
            {
                Thread.Sleep(200);
                if(errores.Count==0 && getModelos())
                {
                    Almacenar();
                }
            }
            Thread.Sleep(100);
            processComplete = true;
            content = processCompleteMsg + System.Environment.NewLine + content;
        }
        catch(Exception exc)
        {
            this.errores.Add(exc.Message + " |MAIN|");
            processComplete = true;
        }
    }

    //-------------------------------------------------------------Funciones principales------------------------------------------

    public Boolean getMarcas()
    {
    try
    {

            int[] claves = { 7657, 7658, 7659, 7660 , 7778 };

            Thread[] threads = new Thread[10];

            _pool = new Semaphore(5, 10);

            foreach (int str in claves)
            {
                content = "INICIANDO la obtención de las marcas con clave  " + str + System.Environment.NewLine + content;
                threads[i] = new Thread(() => getMS(str));
                threads[i].Start();
                i++;
                Thread.Sleep(60);
            }
            i = 0;

            //_pool.Release(10);

            return true;
        }catch(Exception exc)
        {
            this.errores.Add(exc.Message + " |getMarcas|");
            return false;
        }

    }

    public  Boolean getModelos()
    {
        string sal = "";
        try
        {
            Thread[] threads = new Thread[100];
            _pool = new Semaphore(50, 55);

            for(i = 0; i<marcas.Count;)
            {
                for (j = 0; j < 100;)
                {
                    if (i <= marcas.Count - 1)
                    {
                        //content = "INICIANDO la obtención  de los modelos de la marca " + marcas[i].nombre_marca + System.Environment.NewLine + content;
                        threads[j] = new Thread(() => getMD(marcas[i]));
                        threads[j].Start();
                        Thread.Sleep(60);
                    }
                    i++;
                    j++;
                }
            }
            foreach (MArcasAfi marcas in marcas)
            {
                foreach(string año in marcas.años)
                {
                    sal += marcas.clave_marca + "|" + marcas.nombre_marca +"|" + marcas.linea + "|"+ año +"<br>";
                }
            }
            if(sal!="")
               CrearTxt(sal, "AFIRME_MARCAS");
            return true;
        }
        catch(Exception exc)
        {
            this.errores.Add(exc.Message + " |getModelos|");
            return false;
        }

    }
    //--------------------------------------------------------Funciones asincronas-------------------------------------------------

    private void getMD(MArcasAfi marca)
    {
        _pool.WaitOne();
        try
        {
            string action = "https://www.segurosafirme.com.mx/MidasWeb/CotizacionAutoIndividualService?op=getListModelos";
            string soap = @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:cot=""http://segurosafirme.com.mx/cotizacion/cotizacionautoindividual""><soapenv:Header/><soapenv:Body><cot:getListModelos><idLineaNegocio>"+marca.linea+"</idLineaNegocio><idMarca>"+marca.clave_marca+"</idMarca><token>"+this.token+"</token></cot:getListModelos></soapenv:Body></soapenv:Envelope>";

            string sal = SoapConexion(soap, action);

            XDocument doc = XDocument.Parse(sal);
            var list = (from d in doc.Descendants("return")
                        select new
                        {
                            modelos =  d.Value
                        }).ToList();

            string salida = "";
            string aux = "";
            int cont = 0;
            foreach (var str in list)
            {
                JsonTextReader reader = new JsonTextReader(new StringReader(str.modelos));
                while (reader.Read())
                {
                    if (reader.Value != null)
                    {
                        if (cont == 0)
                        {
                            marca.setaño(reader.Value.ToString());
                            cont = 1;
                        }
                        else
                        {
                            cont = 0;
                        }
                    }
                }
            }
        }
        catch (Exception exc)
        {
            this.errores.Add(exc.Message + " |getModelos|");
            content = "error en la obtención  de los modelos de la marca " + marca.nombre_marca + System.Environment.NewLine + System.Environment.NewLine + content;

        }
        finally
        {
            _pool.Release();
            content = "Terminando la obtención de los modelos de la marca " + marca.nombre_marca + System.Environment.NewLine + content;

        }
    }


    public void getMS(int nombres)
    {
        _pool.WaitOne();
        try
        {
            string action = "https://www.segurosafirme.com.mx/MidasWeb/CotizacionAutoIndividualService?op=getListMarcas";
            string soap = @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:cot=""http://segurosafirme.com.mx/cotizacion/cotizacionautoindividual""><soapenv:Header/><soapenv:Body><cot:getListMarcas><!--Optional:--><idLineaNegocio>"+nombres+"</idLineaNegocio><!--Optional:--><token>"+this.token+"</token></cot:getListMarcas></soapenv:Body></soapenv:Envelope>";

            string sal = SoapConexion(soap,action);

            XDocument doc = XDocument.Parse(sal);
            var list = (from d in doc.Descendants("return")
                        select new
                        {
                            marcas = d.Value
                        }).ToList();
            string salida = "";
            string aux =  "";
            int cont = 0;
            foreach (var str in list)
            {
                JsonTextReader reader = new JsonTextReader(new StringReader(str.marcas)); 
                while (reader.Read())
                {
                    if (reader.Value != null)
                    {
                        if (cont == 0)
                        {
                            aux = reader.Value.ToString();
                            cont = 1;
                        }
                        else
                        {
                            marcas.Add(new MArcasAfi(aux, reader.Value.ToString(),nombres));
                            cont = 0;
                        }
                    }
                }
            }

       
        }
        catch(Exception exc)
        {
            this.errores.Add(exc.Message + " |getMarcas|");
            content = "Error en la obtención de los modelos de la marca " + nombres + System.Environment.NewLine + content;

        }
        finally
        {
            //content = "Terminando la obtención de los modelos de la marca " + nombres + System.Environment.NewLine + content;
            _pool.Release();
        }
        


    }

    //-----------------------------------------Funcion de actualizacion asincrona--------------------------------------//
    protected void Timer1_Tick(object sender, EventArgs e)
    {
        if (inProcess)
        {
            string pass = (DateTime.Now - time).ToString();
            lbl1.Text = pass;
            TextBox1.Text = content;
        }
        if (processComplete && content.Contains(processCompleteMsg)) //has final message been set?
        {
            Timer1.Enabled = false;
            inProcess = false;
            Button1.Enabled = true;
            i = 0; j = 0;
        }
        else if (errores.Count > 0)
        {
            Button1.Enabled = true;
            i = 0; j = 0;

        }
    }
    //--------------------------------------------Funciones de utileria-------------------------------------------------------//

    private void CrearTxt(String salida, String name)
    {
        //inicia el intento de crear el archivo
        try
        {
            //genera el archivo dentro del escritorio con el nombre y la cadena dada
            FileStream file = File.Create(this.path + name + ".txt");
            Encoding iso = Encoding.GetEncoding("ISO-8859-1");
            Encoding utf8 = Encoding.UTF8;
            byte[] utfBytes = utf8.GetBytes(salida);
            byte[] isoBytes = Encoding.Convert(utf8, iso, utfBytes);
            file.Write(isoBytes, 0, isoBytes.Length);
            file.Close();
        }
        catch (Exception excep)
        {
            this.errores.Add(excep.Message + "|" + excep.InnerException + "|" + excep.StackTrace);
        }
    }
    //-------------------------------------------Funciones de conexion WS ---------------------------------------------------------
    private string SoapConexion(string soap , string action)
    {
        XmlDocument soapEnv = CrearSoapEnv(soap);
        HttpWebRequest webRequest = CrearWebRequest(this.url, action);
        soapRequest(soapEnv, webRequest);
        IAsyncResult async = webRequest.BeginGetResponse(null, null);

        async.AsyncWaitHandle.WaitOne();
        string sal = "";
        using (WebResponse web = webRequest.EndGetResponse(async))
        {
            using (StreamReader rd = new StreamReader(web.GetResponseStream()))
            {
               return sal = rd.ReadToEnd();
            }
        }
    }


    private void soapRequest(XmlDocument soapEnv, HttpWebRequest webRequest)
    {
        using (Stream stream = webRequest.GetRequestStream())
        {
            soapEnv.Save(stream);
        }

    }

    private HttpWebRequest CrearWebRequest(string url, string action)
    {
        HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create(url);
        webRequest.Headers.Add("SOAPAction", action);
        webRequest.ContentType = "text/xml;charset=\"iso-8859-1\"";
        webRequest.Accept = "text/json";
        webRequest.Method = "POST";
        return webRequest;
    }

    private XmlDocument CrearSoapEnv(string soap)
    {
        XmlDocument soapEnvelopeDocument = new XmlDocument();
        soapEnvelopeDocument.LoadXml(soap);
        return soapEnvelopeDocument;
    }

    //------------------------------------------------FUNCION DE INSERCION--------------------------------------------------------

    private Boolean Almacenar()
    {
        try
        {
            //se crea la conexion a la base de datos , los parametros tienen que cambiarse
            using (var conn = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=Afirme_Catalogos;user id = servici1; password = Master@011"))
            {
                //se accede y se ejecuta a el stop procedure y se le asigna un timeout de 0 para que no interrumpa la conexion
                using (var command = new SqlCommand("[SP_AfirmeCatalogos]", conn) { CommandType = CommandType.StoredProcedure })
                {
                    command.CommandTimeout = 0;
                    conn.Open();
                    command.ExecuteNonQuery();
                    conn.Close();


                    return true;
                }
            }
        }
        catch (Exception excep)
        {
            this.errores.Add(excep.Message + " |Almacenar| \n");
            return false;
        }
    }
}


//------------------------------------------------------Funciones Auxiliares----------------------------------------------
public class MArcasAfi
{
    public string clave_marca { get; set; }
    public string nombre_marca { get; set; }

    public int linea { get; set; }

    public List<string> años = new List<string>();
    public MArcasAfi(string id,string nombre, int linea)
    {
        clave_marca = id;
        nombre_marca = nombre;
        this.linea = linea;
    }

    public void setaño(string año)
    {
        años.Add(año);
    }

}
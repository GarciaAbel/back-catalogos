﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    private string usr= "WSAHORRA";
    private String path = @"C:\Users\ultron\Desktop\";
    private List<String> errores;
    private List<Marcas> marcas;
    private List<SubTipos> submarcas;
    private List<Vehiculos> vehiculos;
    private List<string> track;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public Boolean verificarAsync(string name)
    {
        try
        {
            if (File.Exists(this.path + name + ".txt"))
            {
                track.Add(name);
                return true;
            }
            else
            {
                Insertar(name);
                return false;
            }
        }
        catch (Exception exc)
        {
            errores.Add(exc.Message + " |Verificar|<br>");
            return false;
        }
    }

    public Boolean Insertar(string names)
    {
        try
        {
            if (names == "ATLAS_Subtipos" && marcas.Count == 0)
            {
                if (marcas.Count != 0)
                    return false;

                string tex = File.ReadAllText(this.path + "ATLAS_Marcas" + ".txt").Replace("<br>", "|");
                string[] sal = tex.Split('|');

                for (int i = 0; i < sal.Length - 1;)
                {
                    marcas.Add(new Marcas(Int32.Parse(sal[i]), sal[i + 1], Int32.Parse(sal[i + 2])));
                    i += 3;
                }
            }
            else if (names == "ATLAS_versiones")
            {
                if (submarcas.Count != 0)
                    return false;

                string tex = File.ReadAllText(this.path + "ATLAS_Subtipos" + ".txt").Replace("<br>", "|");
                string[] sal = tex.Split('|');

                for (int i = 0; i < sal.Length - 1;)
                {
                    submarcas.Add(new SubTipos(Int32.Parse(sal[i]), Int32.Parse(sal[i + 1]), sal[i + 2], Int32.Parse(sal[i + 3])));
                    i += 4;
                }


            }
            return false;
        }
        catch (Exception exc)
        {
            errores.Add(exc.Message + " ||<br>");
            return false;
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        errores = new List<String>();
        marcas = new List<Marcas>();
        submarcas = new List<SubTipos>();
        vehiculos = new List<Vehiculos>();
        track = new List<string>();
    
    string sal = "";
        doThings();

        if (errores.Count != 0)
        {
            foreach (string str in errores)
            {
                sal += str;
            }
        }
        else
            sal = "se a completado con exito";


        Label1.Text = sal;
    }

    public Boolean doThings()
    {
        string[] names = { "ATLAS_Marcas", "ATLAS_Subtipos", "ATLAS_versiones" };
        string sal = "";
        try
        {
            ATLAS.WSSegAtlasImplClient atlas = new ATLAS.WSSegAtlasImplClient();

            if (errores.Count == 0 && (verificarAsync(names[0]) || getMarcas(atlas)))
            {
                if (errores.Count == 0 && (verificarAsync(names[1]) || getSubtipo(atlas)))
                {
                    if (errores.Count == 0 && (verificarAsync(names[2]) || getVersion(atlas)))
                    {
                        if (errores.Count == 0 && (getModelo(atlas)))
                        {
                            if(track.Count==3)
                            {
                                delete();
                            }
                            else
                            {
                                Almacenar();
                            }
                            return true;
                        }
                    }
                }

            }
            return false;
        }
        catch (Exception exc)
        {
            errores.Add(exc.Message + " |Inicio| <br>");
            return false;
        }
    }

    public void delete()
    {
        foreach (string str in track)
        {
            File.Delete(path + str + ".txt");
        }
    }

    public Boolean getModelo(ATLAS.WSSegAtlasImplClient atlas)
    {
        if (vehiculos.Count == 0)
            return false;

        int i = 0;

        string sal = "";
        try
        {
            ATLAS.Record6[] records = new ATLAS.Record6[1];
            records[0] = new ATLAS.Record6();
            records[0].VG_Usuario = this.usr;
            records[0].VG_Password = this.usr;
            foreach (Vehiculos vehi in vehiculos)
            {  
                records[0].cve_vehiculo = vehi.id_vehiculo;
                if (vehi.clasif == 1)
                    records[0].num_liga = 1001054;
                else
                    records[0].num_liga = 1001177;
                try
                {
                    ATLAS.Record[] modelos = atlas.WSA_CatModelos(records);

                    if(modelos!=null)
                    {
                        foreach (ATLAS.Record mod in modelos)
                        {
                            sal += vehi.id_marca + "|" + vehi.id_sub + "|" + vehi.id_vehiculo + "|" + vehi.amis + "|" + vehi.nombre + "|" + vehi.fecha_alta + "|" + mod.cve_marca + "|" + vehi.clasif + "<br>";
                        }
                    }
                    else
                    {
                            sal += vehi.id_marca + "|" + vehi.id_sub + "|" + vehi.id_vehiculo + "|" + vehi.amis + "|" + vehi.nombre + "|" + vehi.fecha_alta + "|" + "0" + "|" + vehi.clasif + "<br>";
                    }

                }
                catch (Exception exc)
                {
                    errores.Add(exc.Message + " |Get Modelos interno| no hay modelos para: " + vehi.nombre + "  <br>");
                }

                i++;
            }
            CrearTxt(sal, "ATLAS_versiones");
            //Label2.Text = sal;
            return true;
        }catch(Exception exc)
        {
            errores.Add(exc.Message + " |Get Modelos| <br>");
            return false;
        }
    }

    public Boolean getVersion(ATLAS.WSSegAtlasImplClient atlas)
    {
        if (submarcas.Count == 0)
            return false;

        string sal = "";

        try
        {
            ATLAS.Record5[] records = new ATLAS.Record5[1];
            records[0] = new ATLAS.Record5();
            records[0].VG_Usuario = this.usr;
            records[0].VG_Password = this.usr;

            foreach (SubTipos sub in submarcas)
        {                
                records[0].cve_categoria = sub.clasif;
                records[0].cve_marca = sub.id_marca;
                records[0].subtipo = sub.id_sub;
                try
                {
                    ATLAS.Record4[] record = atlas.WSA_CatVehiculos(records);

                    foreach (ATLAS.Record4 record4 in record)
                    {
                         vehiculos.Add(new Vehiculos(sub.id_marca,sub.id_sub,(int)record4.cve_vehiculo,record4.cve_amis,record4.des_vehiculo,record4.fecalta,sub.clasif));
                        //sal += sub.id_marca + "|" + sub.id_sub + "|" + record4.cve_vehiculo + "|" + record4.cve_amis + "|" + record4.des_vehiculo + "|" + record4.fecalta + "|" + sub.clasif + "<br>"; 
                    }
                }
                catch (Exception exc)
                {
                    //if(sub.clasif!=4)
                    //{
                        //errores.Add(exc.Message + " |Get versiones interno| no hay versiones para: " + sub.nombre + "  <br>");
                        //return false;
                    //}
                }
            }
            //Label2.Text = sal;
            //CrearTxt(sal, "ATLAS_versiones"); 
            return true;

        }
        catch (Exception exc)
        {
            errores.Add(exc.Message + " |Get versiones| <br>");
            return false;
        }

    }

    public Boolean getSubtipo(ATLAS.WSSegAtlasImplClient atlas)
    {
        if (marcas.Count == 0)
            return false;

        string sal = "";
        try
        {
            ATLAS.Record3[] record3 = new ATLAS.Record3[1];
            record3[0] = new ATLAS.Record3();
            record3[0].VG_Usuario = this.usr;
            record3[0].VG_Password = this.usr;

            foreach (Marcas marc in marcas)
            {
                record3[0].cve_armadora = marc.id;
                record3[0].cve_categoria = marc.clasif;
                try
                {
                    ATLAS.Record2[] records = atlas.WSA_CatSubTipos(record3);

                    foreach (ATLAS.Record2 record in records)
                    {
                        submarcas.Add(new SubTipos(marc.id,(int)record.cve_subtipo,record.descripcion,(int)record.cve_categoria));
                        sal += marc.id + "|" + record.cve_subtipo + "|" + record.descripcion + "|" + record.cve_categoria + "<br>";
                    }
                }
                catch(Exception exc)
                {
                    if (marc.clasif != 4)
                    {
                        errores.Add(exc.Message + " |Get subtipos interno| no hay subtipos para: " + marc.nombre + "  <br>");
                        return false;
                    }
                }

            }

            CrearTxt(sal,"ATLAS_Subtipos");
            return true;
        }
        catch (Exception exc)
        {
            errores.Add(exc.Message + " |Get subtipos| <br>");
            return false;
        }
    }

    public Boolean getMarcas(ATLAS.WSSegAtlasImplClient atlas)
    {
        string salida = "";
        int[] clasif = {1,2,4};
        int i = 0;

        try
        {
            ATLAS.Record1[] record1 = new ATLAS.Record1[1];
            record1[0] = new ATLAS.Record1();
            record1[0].VG_Password = this.usr;
            record1[0].VG_Usuario = this.usr;

            foreach (int j in clasif)
            {
                record1[0].cve_clasif = j;
                 ATLAS.Record[] rec = atlas.WSA_CatMarcas(record1);

                foreach (ATLAS.Record record in rec)
                {
                    //if(j!=4)
                    marcas.Add(new Marcas((int)record.cve_marca, record.des_marca, j));

                    salida += record.cve_marca + "|" + record.des_marca + "|" + j + "<br>";
                    i++;

                    
                }
            }

            CrearTxt(salida, "ATLAS_Marcas");
            return true;
        }
        catch (Exception exc)
        {
            errores.Add(exc.Message + " |Get Marcas| <br>");
            return false;
        }
    }

    private void CrearTxt(String salida, String name)
    {
        //inicia el intento de crear el archivo
        try
        {
            //genera el archivo dentro del escritorio con el nombre y la cadena dada
            FileStream file = File.Create(this.path + name + ".txt");
            Byte[] info = new UTF8Encoding(true).GetBytes(salida);
            file.Write(info, 0, info.Length);
            file.Close();
        }
        catch (Exception excep)
        {
            this.errores.Add(excep.Message + "|" + excep.InnerException + "|" + excep.StackTrace);
        }
    }


    private Boolean Almacenar()
    {
        try
        {
            //se crea la conexion a la base de datos , los parametros tienen que cambiarse
            using (var conn = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=ExtraccionAutos;user id = servici1; password = Master@011"))
            {
                //se accede y se ejecuta a el stop procedure y se le asigna un timeout de 0 para que no interrumpa la conexion
                using (var command = new SqlCommand("[SP.AtlasCatalogos]", conn) { CommandType = CommandType.StoredProcedure })
                {
                    command.CommandTimeout = 0;
                    conn.Open();
                    command.ExecuteNonQuery();
                    conn.Close();


                    return true;
                }
            }
        }catch(Exception excep)
        {
            this.errores.Add(excep.Message + " |Almacenar| \n");
            return false;
        }


    }
}


public class Marcas
{
    public int id { set; get; }
    public string nombre { set; get; }
    public int clasif { set; get; }

    public Marcas(int id , string nombre, int clasif )
    {
        this.id = id;
        this.nombre = nombre;
        this.clasif = clasif;
    }
}

public class SubTipos
{
    public int id_marca { set; get; }
    public int id_sub { set; get; }
    public string nombre { set; get; }
    public int clasif { set; get; }

    public SubTipos(int id_marca , int id_sub, string nombre, int clasif)
    {
        this.id_marca = id_marca;
        this.id_sub = id_sub;
        this.nombre = nombre;
        this.clasif = clasif;
    }

}

public class Vehiculos
{
    public int id_marca { set; get; }
    public int id_sub { set; get; }
    public int id_vehiculo { set; get; }
    public string amis { set; get; }
    public string nombre { set; get; }
    public string fecha_alta { set; get; }
    public int clasif { set; get; }

    public Vehiculos(int id_marca, int id_sub,int id_vehiculo, string amis, string nombre,string fecha_alta ,int clasif)
    {

        this.id_marca = id_marca;
        this.id_sub = id_sub;
        this.id_vehiculo = id_vehiculo;
        this.amis = amis;
        this.nombre = nombre;
        this.fecha_alta = fecha_alta;
        this.clasif = clasif;
    }

}
﻿//Garcia Sanchez Alberto Abel

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    //Funcion principal para comenzar contiene las credenciales 
    protected void Button1_Click(object sender, EventArgs e)
    {
        //creacion del servicio web 
        ElAguila.CatCotizadorClient aguila = new ElAguila.CatCotizadorClient();
        //password del aguila
        String password = "AgtTrigarante2017#%";
        //cadenas de status
        String SalidaCreacion = "";
        String SalidaAlmacenaje = "";

        //Lista de marcas para el primer catalogo
        List<Marca> listaMarca = ObtenerMarcas(password, aguila);

        //Lista de nombres de los stored procedure
        String[] names = { "Sp_ELAGUILA_CATALOGO" , "Sp_ELAGUILA_SUBMARCA" , "Sp_ELAGUILA_SUBMARCA_TIPO"};

        //si no se obtuvo ninguna salida en la lista de marcas del primer catalogo no entra 
        if (listaMarca.Count != 0)
        {
            //lista de marcas del segundo catalogo , utiliza la primera lista generada para generar la segunda
            List<Marca> listaSubMarca = ObtenerSubMarcas(listaMarca, aguila, password);

            //si la segunda lista esta vacia no entra
            if (listaSubMarca.Count != 0)
            {
                //condicional con la funcion que genera las listas del tercer catalogo , utiliza las primeras dos listas para su generacion 
                if(ObtenerSubMarcasTipo(listaMarca,listaSubMarca, aguila, password))
                {
                    //si la salida fue exitosa 
                    SalidaCreacion += "Se crearon los archivos correctamente";
                }
                else
                {
                    //si la salida fue erronea
                    SalidaCreacion += "ah habido un error en la creacion de los archivos";
                }
            }
        }
        else
        {
            //si no se genero la primera lista salta este error
            SalidaCreacion += "hubo un error al generar los archivos";
        }

        foreach (String name in names)
        {
            if (Almacenar(name) == "ok")
                SalidaAlmacenaje += "Se almacenaron los archivos correctamente";
            else
                SalidaAlmacenaje += "hubo un error al momento de almacenar";
        }

        Label1.Text = SalidaCreacion;
        Label2.Text = SalidaAlmacenaje;
        //---------------------------------------AXXA-------------------------------------------------
        

        //---------------------------------------Mapfre-------------------------------------------------

        MapfreWS.CatalogosAutosSoapClient service = new MapfreWS.CatalogosAutosSoapClient();
        Label1.Text = service.WS_TW_Marcas("<![CDATA[<xml><data><valor cod_ramo='45' cod_zona_agt='99' cod_tip_vehi='5' anio_fabrica='2010' id_negocio='LOCALES'/></data></xml>]]>", "20190724").OuterXml.ToString();
    }

    //consume un servicio web y obtiene los catalogos de las marcas, recibe la contraseña y el objeto del web service 
    public List<Marca> ObtenerMarcas(String password, ElAguila.CatCotizadorClient aguila)
    {
        //Cadena con los datos de salida
        String salida = "";

        //lista de objetos de tipo marca , sirve para guardar los resultados
        List<Marca> listS = new List<Marca>();

        //bandera que sirve para saber si un año ya a sido integrado en una marca
        int flag = 0;

        //iteracion que va desde 1981(primer año de registro en el catalogo de autos) , hasta el ultimo año
        for (int i = 1900; i < DateTime.Now.Year + 2; i++)
        {
            //Creacion de arreglo de objetos de tipo TypCatalogString y consumo de web service a la funcion marca
            ElAguila.TypCatalogString[] Agstr = aguila.Marca(password, i, "R");

            //iteracion del arreglo de objetos dentro de Agstr
            foreach (ElAguila.TypCatalogString str in Agstr)
            {
                //Si la lista se encuentra vacia insertar el primer elemento sin validaciones
                if (listS.Count() == 0)
                {
                    //se crea un objeto de tipo marca para insertarlo en la lista
                    Marca marca = new Marca(str.strCve, str.strDesc, i.ToString() , 0);
                    listS.Add(marca);
                }
                //si ya hay elementos dentro de la lista entra a esta parte
                else
                {
                    //itera la lista para buscar si ya hay una marca registrada 
                    foreach (Marca marc in listS)
                    {
                        //si hay una marca registrada que empate con la que esta siendo revisada actualmente entra aqui
                        if (marc.clv == str.strCve)
                        {
                            //se modifica el año dentro de la lista y se agrega , despues la bandera se pone en 1 y se rompe
                            marc.ModificarAnio(i.ToString());
                            flag = 1;
                            break;
                        }
                    }
                    //si recorrio toda la lista y no hubo ninguna marca que empatara se crea y agrega otra marca a la lista 
                    if (flag == 0)
                    {
                        Marca marca = new Marca(str.strCve, str.strDesc, i.ToString(), 0);
                        listS.Add(marca);
                    }
                    //si no , se pone la bandera de nuevo en 0
                    else
                        flag = 0;
                }
            }
        }

        //se itera la lista de tipo marca y se colocan sus atributos dentro de una cadena 
        foreach (Marca marca in listS)
        {
            salida += marca.clv + "|" + marca.desc + "|" + marca.anios + "<br>";
        }

        //se manda la cadena y un nombre que se le va a dar al archivo que genera la funcion crearTxt
        CrearTxt(salida, "MarcaElAguila");
        
            //se regresa la lista
            return listS;
    }

    //consume un servicio web y obtiene los catalogos de las submarcas, recibe la contraseña, el objeto del web service ademas de la lista de marcas 
    public List<Marca> ObtenerSubMarcas(List<Marca> lista, ElAguila.CatCotizadorClient aguila, String pass)
    {
        //se crea una lista de objetos de tipo marca
        List<Marca> submarca = new List<Marca>();
        //se crea una cadena vacia 
        String salida = "";

        //se itera por cada marca en la lista de marcas
        foreach (Marca marc in lista)
        {
            //se obtiene cada uno de los años dentro de cada marca
            String[] cadenas = marc.anios.Split(',');

            //por cada año dentro de los años de cada marca
            foreach (String str in cadenas)
            {
                int clave = int.Parse(str);

                //Creacion de arreglo de objetos de tipo TypCatalogString y consumo de web service a la funcion subMarca
                ElAguila.TypCatalogString[] Agstr = aguila.SubMarca(pass, clave, int.Parse(marc.clv), "R", marc.desc);

                //se itera la lista de objetos por cada objeto en Agstr
                foreach (ElAguila.TypCatalogString strA in Agstr)
                {
                    //por cada resultado se genera un objeto de tipo marca, y se agregan a la lista , ademas se concatenan los resultados en la cadena de salida
                    Marca marcas = new Marca(strA.strCve, strA.strDesc, str, int.Parse(marc.clv));
                    submarca.Add(marcas);
                    salida += marcas.clv + "|" + marcas.desc + "|" + marcas.anios + "|" + marc.clv + "<br>";
                }
            }
        }

        //se crea el archivo llamado submarca
        CrearTxt(salida, "submarca");

        //se regresa la lista
        return submarca;

    }

    //consume un servicio web y obtiene los catalogos de las descripciones de las sub marcas, recibe la contraseña, el objeto del web service, la lista de marcas y sub marcas 
    public Boolean ObtenerSubMarcasTipo(List<Marca> lista, List<Marca> Sublista, ElAguila.CatCotizadorClient aguila, String pass)
    {
        String salida = "";

        //se crea un entero con un valor de -1 para poder comparar la marca actual con la que se va a comparar
        int clvMarca = -1;
        //se crea un string donde se va a almacenar el nombre de la marca
        String nombre = "";

        //por cada marca en la lista de submarcas
        foreach (Marca marcas in Sublista)
        {
            //si la clave de la marca actual es diferente a la clave que se va a trabajar , se busca en nombre en la lista de marcas y se asigna a la cadena nombre
            if (clvMarca != marcas.clv_marca)
                nombre = find(lista, marcas.clv_marca);

            //Creacion de arreglo de objetos de tipo TypCatalogString y consumo de web service a la funcion SubMarcaTipo
            ElAguila.TypCatalogString[] astr = aguila.SubMarcaTipo(pass, int.Parse(marcas.anios), "R", marcas.clv_marca, nombre, int.Parse(marcas.clv), "?");

            //por cada objeto dentro de astr
            foreach (ElAguila.TypCatalogString agstr in astr)
            {
                //se obtienen los datos almacenados en los objetos dentro de astr y se concatenan en la cadena salida
                salida += agstr.strCve + "|" + agstr.strDesc + "|" + marcas.anios + "|" + marcas.clv_marca + "|" + marcas.clv + "<br>";
            }
        }

        //se crea el archivo con la cadena salida y el nombre submarcatipo
        CrearTxt(salida, "SubMarcaTipo");

        //regresa true si se completo bien la operacion
        return true;
    }

    //busca en una lista de objetos de tipo marca si se encuentra una marca con una cierta clave , recibe una lista de marcas y una clave de marca
    public String find(List<Marca> lista, int marca)
    {
        //itera por cada objeto dentro de la lista de marcas
        foreach (Marca marc in lista)
        {
            //si se encontro una coincidencia se regresa el nombre de la marca
            if (int.Parse(marc.clv) == marca)
                return marc.desc;
        }

        //si no , regresa la palabra false
        return "false";
    }

    //crea un archivo de texto dentr de el escritorio, recive una cadena con los datos a poner en el archivo y el nombre del archivo
    public void CrearTxt(String salida,String name)
    {
        //inicia el intento de crear el archivo
        try
        {
            //genera el archivo dentro del escritorio con el nombre y la cadena dada
            FileStream file = File.Create(@"C:\Users\ultron\Desktop\"+name+".txt");
            Byte[] info = new UTF8Encoding(true).GetBytes(salida);
            file.Write(info, 0, info.Length);
            file.Close();
        }
        catch (Exception exc)
        {
            
        }
    }
    //inicia una conexion con la base de datos (sql server) y manda llamar a un stored procedure de la misma 
    public String Almacenar(String store)
    {
        try
        {
            //se crea la conexion a la base de datos , los parametros tienen que cambiarse
            using (var conn = new SqlConnection(@"Data Source=DESKTOP-D03IEPJ\SQLEXPRESS;Initial Catalog=ElAguilaCatalogos;Integrated Security = True"))
            {
                //se accede y se ejecuta a el stop procedure y se le asigna un timeout de 0 para que no interrumpa la conexion
                using (var command = new SqlCommand(store, conn) { CommandType = CommandType.StoredProcedure })
                {
                    command.CommandTimeout = 0;
                    conn.Open();
                    command.ExecuteNonQuery();
                    conn.Close();


                    return "ok";
                }
            }
        }
        catch (Exception excep)
        {
            return excep.Message;
        }
    }

}

//clase Marca
public class Marca
{
    //parametros dentro de la misma clase marca
    public String clv { get; set; }
    public String desc { get; set; }
    public String anios { get; set; }
    public int clv_marca { get; set; }

    //constructor de la clase
    public Marca(String clave, String desc, String anios , int clv_marca)
    {
        this.clv = clave;
        this.desc = desc;
        this.anios = anios;
        this.clv_marca = clv_marca;
    }
    
    //funcion modificaranios recibe un string y lo concatena con el atributo anios
    public void ModificarAnio(String anio)
    {
        this.anios += ","+anio;
    }

}
﻿//Garcia Sanchez Alberto Abel

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Serialization;

public partial class _Default : System.Web.UI.Page
{
    private String usr = "linea";
    private String pass = "linea";
    private String path = @"C:\Users\ultron\Desktop\";
    private List<String> errores;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        errores = new List<string>();
        String salida = "";
        try
        {
            QualitasSoap.wsTarifaSoapClient qualitas = new QualitasSoap.wsTarifaSoapClient();

            if (getMarcas(qualitas) && errores.Count == 0)
            {
                if(getTarifas(qualitas) && errores.Count == 0)
                {

                }
            }
        } catch (Exception exc)
        {
            errores.Add(exc.Message + " |instancia WS| \n");
        }

        if (this.errores.Count != 0)
        {
            foreach (String str in errores)
                salida += str + "<br>";
            Label1.Text = salida;
        }
        else
            Label1.Text = "Se a completado con exito";

    }


    public Boolean getMarcas(QualitasSoap.wsTarifaSoapClient qualitas)
    {
        string salida = "";
        try
        {
            salida sal = deserializarMarcas(qualitas.listaMarcas(this.usr, this.pass));
            foreach(Elemento elm in sal.elementos)
            {
                salida += elm.cMarca + "|" + elm.cMarcaLarga + "<br>";
            }
            CrearTxt(salida,"Qualitas_marcas");
            return true;

        } catch (Exception exp)
        {
            this.errores.Add(exp.Message + " |getMarcas| \n");
            return false;
        }
    }

    public Boolean getTarifas(QualitasSoap.wsTarifaSoapClient qualitas)
    {
        string salida = "";
        try
        {
            salida sal = deserializarMarcas(qualitas.listaTarifas(this.usr, this.pass, "", "", "", "", "", "", ""));
            foreach(Elemento elem in sal.elementos)
            {
                salida += elem.CAMIS + "|" + elem.cCategoria + "|" + elem.cNvaAMIS + "|" + elem.cMarca + "|" + elem.cMarcaLarga + "|" + elem.cModelo + "|" + elem.cTarifa + "|" + elem.cTipo + "|" + elem.cVersion + "|" + elem.cOcupantes + "|" + elem.cTransmision + "|" + elem.nV1 + "|" + elem.nV2 + "<br>";
            }

            //Label2.Text = salida;
            CrearTxt(salida, "Qualitas_descripciones");

        }catch(Exception exc)
        {
            Label2.Text = exc.Message;
        }

        return true;
    }

    public salida deserializarMarcas(System.Xml.Linq.XElement name)
    {
        salida catalogo = null;
        XmlSerializer serial = new XmlSerializer(typeof(salida));
        try
        {
            using (var reader = new StringReader(name.ToString()))
            {
                catalogo = (salida)serial.Deserialize(reader);
                return catalogo;
            }
        }
        catch (Exception exv)
        {
            this.errores.Add(exv.Message+" |deserializacion| \n");
            return catalogo;
        }

    }

    private void CrearTxt(String salida, String name)
    {
        //inicia el intento de crear el archivo
        try
        {
            //genera el archivo dentro del escritorio con el nombre y la cadena dada
            FileStream file = File.Create(this.path + name + ".txt");
            Byte[] info = new UTF8Encoding(true).GetBytes(salida);
            file.Write(info, 0, info.Length);
            file.Close();
        }
        catch (Exception excep)
        {
            this.errores.Add(excep.Message + "|" + excep.InnerException + "|" + excep.StackTrace);
        }
    }

    private Boolean Almacenar()
    {
        try
        {
            //se crea la conexion a la base de datos , los parametros tienen que cambiarse
            using (var conn = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=QualitasCatalogos;user id = servici1; password = Master@011"))
            {
                //se accede y se ejecuta a el stop procedure y se le asigna un timeout de 0 para que no interrumpa la conexion
                using (var command = new SqlCommand("[SP.QualitasCatalogos]", conn) { CommandType = CommandType.StoredProcedure })
                {
                    command.CommandTimeout = 0;
                    conn.Open();
                    command.ExecuteNonQuery();
                    conn.Close();


                    return true;
                }
            }
        }
        catch (Exception excep)
        {
            this.errores.Add(excep.Message + " |Almacenar| \n");
            return false;
        }
    }
}

[Serializable()]
[System.Xml.Serialization.XmlRoot("salida")]
public class salida
{
    [XmlArray("datos")]
    [XmlArrayItem("Elemento", typeof(Elemento))]
    public Elemento[] elementos { get; set; }
}

[Serializable()]
public class Elemento
{
    [System.Xml.Serialization.XmlElement("cMarca")]
    public string cMarca { get; set; }
    [System.Xml.Serialization.XmlElement("cMarcaLarga")]
    public string cMarcaLarga { get; set; }

    [System.Xml.Serialization.XmlElement("cTarifa")]
    public string cTarifa { get; set; }

    [System.Xml.Serialization.XmlElement("cTipo")]
    public string cTipo { get; set; }

    [System.Xml.Serialization.XmlElement("cVersion")]
    public string cVersion { get; set; }

    [System.Xml.Serialization.XmlElement("cModelo")]
    public string cModelo { get; set; } 
    [System.Xml.Serialization.XmlElement("CAMIS")]
    public string CAMIS { get; set; }

    [System.Xml.Serialization.XmlElement("cCategoria")]
    public string cCategoria { get; set; }

    [System.Xml.Serialization.XmlElement("cTransmision")]
    public string cTransmision { get; set; }

    [System.Xml.Serialization.XmlElement("cOcupantes")]
    public string cOcupantes { get; set; }

    [System.Xml.Serialization.XmlElement("nV1")]
    public string nV1 { get; set; }

    [System.Xml.Serialization.XmlElement("nV2")]
    public string nV2 { get; set; }

    [System.Xml.Serialization.XmlElement("cNvaAMIS")]
    public string cNvaAMIS { get; set; }
}

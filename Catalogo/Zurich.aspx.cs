﻿using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml.Serialization;

public partial class _Default : System.Web.UI.Page
{

    private String usr = "tri74066ws";
    private String pass = "tri74066ws";
    private int agente = 74066;
    private int relacion = 8701022;
    private String path = @"C:\Users\ultron\Desktop\";
    private List<String> errores;
    private List<marca> marcas;
    private List<submarca> submarcas;
    private List<usos> usos;
    private List<string> track;
    private List<modelos> Modelos ;

    public string catalogos = "http://pruebas.autolinea.ezurich.com.mx:80/ZurichWS_QA/autos/consultaCatalogosAutos/publicService";
    public string claves = "http://pruebas.autolinea.ezurich.com.mx:80/ZurichWS_QA/autos/consultaClavesVehiculos/publicService";

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
         errores = new List<String>();
         marcas = new List<marca>();
         submarcas = new List<submarca>();
         usos = new List<usos>();
         track = new List<string>();
         Modelos = new List<modelos>();

    String error = "";
        int intentos = 0;

        do
        {
            track.Clear();
            this.errores.Clear();
            marcas.Clear();
            Modelos.Clear();
            submarcas.Clear();
            usos.Clear();

            if (doGeneracion())
            {
                error = "Se a completado cn exito";
                break;
            }
            else
            {
                foreach (String str in this.errores)
                    error += str + "|Intento: " + intentos + "|<br>";


                intentos++;
                this.errores.Clear();
               }

        } while (intentos < 1);

        Label1.Text = error;
    }

    public Boolean insertar(string name)
    {
        if(name == "Zurich_Sub_Marcas")
        {
            if (marcas.Count != 0)
                return false;

            string tex = File.ReadAllText(this.path + "Zurich_Marcas" + ".txt").Replace("<br>", "|");
            string[] sal = tex.Split('|');

            for (int i = 0; i < sal.Length-1;)
            {
                this.marcas.Add(new marca(Int32.Parse(sal[i]),sal[i+1],sal[i + 2]));
                i += 3;
            }

            return true;
        }

        if (name == "Zurich_descripciones")
        {
            if (Modelos.Count != 0)
                return false;

            string tex = File.ReadAllText(this.path + "Zurich_Modelos" + ".txt").Replace("<br>", "|");
            string[] sal = tex.Split('|');

            for (int i = 0; i < sal.Length - 1;)
            {
                this.Modelos.Add(new modelos(Int32.Parse(sal[i]),Int32.Parse(sal[i+1]),sal[i+2],Int32.Parse(sal[i+3]),sal[i+4]));
                i += 5;
            }

            return true;
        }

        if (name == "Zurich_Modelos")
        {
            if (submarcas.Count != 0)
                return false;


            string tex = File.ReadAllText(this.path + "Zurich_Sub_Marcas" + ".txt").Replace("<br>","|");
            string[] sal = tex.Split('|');

            for(int i = 0; i< sal.Length-1;)
            {
                this.submarcas.Add(new submarca(Int32.Parse(sal[i+1]), sal[i + 2], Int32.Parse(sal[i]),sal[i+3]));
                i+=4;
            }

            return true;
        }

        if (name == "Zurich_Cargas")
        {
            if (submarcas.Count != 0)
                return false;


            string tex = File.ReadAllText(this.path + "Zurich_Usos" + ".txt").Replace("<br>", "|");
            string[] sal = tex.Split('|');

            for (int i = 0; i < sal.Length-1;)
            {
                this.usos.Add(new usos(Int32.Parse(sal[i]),sal[i+1]));
                i += 2;
            }

            return true;
        }

        return false;
    }

    public Boolean verificar(string name)
    {
        try
        {
            if (File.Exists(this.path + name + ".txt"))
            {

                track.Add(name);
                return true;
            }
            else
            {
                insertar(name);
                return false;
            }
        }
        catch (Exception exc)
        {
            errores.Add(exc.Message + " |Verificar|<br>");
            return false;
        }
    }

    public void delete()
    {
        foreach(string str in track)
        {
            File.Delete(path + str + ".txt");
        }
    }

    public Boolean doGeneracion()
    {
        string[] nombres = { "Zurich_Marcas", "Zurich_Sub_Marcas", "Zurich_Modelos", "Zurich_Usos", "Zurich_Cargas" , "Zurich_Tipos", "Zurich_Tipo_Valor" , "Zurich_descripciones" };
        try
        {
            if ((errores.Count == 0) && (verificar(nombres[0]) || getMarcas()))
            {
                if ((errores.Count == 0) && (verificar(nombres[1]) || getSubMarcas()))
                {
                    if ((errores.Count == 0) && (verificar(nombres[2]) || getModelos()))
                    {
                        if ((errores.Count == 0) && (verificar(nombres[7]) || getDescripciones()))
                        {
                            if ((errores.Count == 0) && (verificar(nombres[3]) || tipoUso()))
                            {
                                if ((errores.Count == 0) && (verificar(nombres[4]) || tipoCarga()))
                                {
                                    if ((errores.Count == 0) && (verificar(nombres[5]) || tipoVehiculo()))
                                    {
                                        if ((errores.Count == 0) && (verificar(nombres[6]) || tipoValor()))
                                        {
                                            if (track.Count == 8)
                                            {
                                                delete();
                                                return false;
                                            }
                                            else
                                            {
                                                Almacenar();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception exc)
        {
            this.errores.Add(exc.Message + " |Inicio| \n");
        }

        if (this.errores.Count != 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public Boolean getMarcas()
    {
        String sal = "";
        string[] tipo = { "1","2","3","4","5","8"};
        try
        {
            foreach(string c in tipo)
            {
                String salida = ApiConsume("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservices.zurich.com/\" ><soapenv:Header><wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" ><UsernameToken xmlns=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" ><Username>" + this.usr + "</Username><Password>" + this.usr + "</Password></UsernameToken></wsse:Security></soapenv:Header><soapenv:Body><web:SolicitudCatalogoMarcas><web:numRequest>11</web:numRequest><web:catalogo>MARCA</web:catalogo><web:usuario>" + this.usr + "</web:usuario><web:agente>" + this.agente + "</web:agente><web:numRelacion>8701022</web:numRelacion><web:tipoVehiculo>"+c+"</web:tipoVehiculo></web:SolicitudCatalogoMarcas></soapenv:Body></soapenv:Envelope>",this.catalogos);
                XDocument doc = XDocument.Parse(salida);
                XNamespace metaData = "http://webservices.zurich.com/";

                var list = (from d in doc.Descendants(metaData + "marca")
                            select new
                            {
                                claveMarca = d.Element(metaData + "claveMarca").Value,
                                descripcion = d.Element(metaData + "descripcion").Value
                            }).ToList();

                foreach (var lista in list)
                {
                    marcas.Add(new marca(Int32.Parse(lista.claveMarca), lista.descripcion, c));
                    sal += lista.claveMarca + "|" + lista.descripcion + "|" + c + "<br>";
                }
            }
            CrearTxt(sal, "Zurich_Marcas");
           return true;
        }
        catch (Exception exc)
        {
            this.errores.Add(exc.Message + " |getMarcas| \n");
            return false;
        }    
    }

    public Boolean getSubMarcas()
    {
        if (marcas.Count == 0)
            return false;

        String salida = "";
        string sal = "";
        int i = 0;
        try
        {
            foreach(marca marc in marcas)
            {
                salida = ApiConsume("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservices.zurich.com/\" ><soapenv:Header><wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" ><UsernameToken xmlns=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" ><Username>" + this.usr + "</Username><Password>" + this.usr + "</Password></UsernameToken></wsse:Security></soapenv:Header><soapenv:Body><web:reqCatSubMarcasAuto><web:numRequest>12</web:numRequest><web:catalogo>SUBMA</web:catalogo><web:usuario>"+this.usr+"</web:usuario><web:agente>"+this.agente+"</web:agente><web:claveMarca>"+marc.claveMarca+"</web:claveMarca><web:numRelacion>8701022</web:numRelacion><web:tipoVehiculo>"+marc.tipoVehiculo+"</web:tipoVehiculo></web:reqCatSubMarcasAuto></soapenv:Body></soapenv:Envelope>",this.catalogos);
                XDocument doc = XDocument.Parse(salida);
                XNamespace metaData = "http://webservices.zurich.com/";

                var list = (from d in doc.Descendants(metaData + "subMarcaAuto")
                            select new
                            {
                                claveSubMarcaAuto = d.Element(metaData + "claveSubMarcaAuto").Value,
                                descripcion = d.Element(metaData + "descripcion").Value
                            }).ToList();

                foreach (var lista in list)
                {
                    submarcas.Add(new submarca(Int32.Parse(lista.claveSubMarcaAuto), lista.descripcion , marc.claveMarca , marc.tipoVehiculo));
                    sal += marc.claveMarca+"|"+lista.claveSubMarcaAuto + "|" + lista.descripcion + "|"+ marc.tipoVehiculo  +"<br>";
                }
                i++;

            }
            CrearTxt(sal, "Zurich_Sub_Marcas");

//            Label2.Text = salida;
            return true;

        }
        catch (Exception exc)
        {
            errores.Add(exc.Message + "|getSubMarcas| + <br>");
            return false;
        }
    }

    public Boolean getModelos()
    {
        if (submarcas.Count == 0)
            return false;

        string salida = "";
        string sal = "";
        int i = 0;
        int j = 0; 
        try
        {
            foreach(submarca sub in submarcas)
            {
                salida = ApiConsume("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservices.zurich.com/\" ><soapenv:Header><wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" ><UsernameToken xmlns=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" ><Username>" + this.usr + "</Username><Password>" + this.usr + "</Password></UsernameToken></wsse:Security></soapenv:Header><soapenv:Body><web:SolicitudCatalogoModelos><web:numRequest>13</web:numRequest><web:catalogo>MODEL</web:catalogo><web:cveMarca>"+sub.claveMarca+"</web:cveMarca><web:cveSubMarca>"+sub.claveSubMarcaAuto+"</web:cveSubMarca><web:numRelacion>8701022</web:numRelacion><web:usuario>"+this.usr+"</web:usuario><web:agente>"+this.agente+"</web:agente><web:tipoVehiculo>"+sub.tipoVehiculo+"</web:tipoVehiculo></web:SolicitudCatalogoModelos></soapenv:Body></soapenv:Envelope>",this.catalogos);
                XDocument doc = XDocument.Parse(salida);
                XNamespace metaData = "http://webservices.zurich.com/";
                XNamespace soap = "http://schemas.xmlsoap.org/soap/envelope/";
                var list = (from d in doc.Element(soap + "Envelope").Element(soap + "Body").Element(metaData + "RespuestaCatalogoModelos").Elements(metaData + "modelo")
                            select new  {
                                modelo =  d.Element(metaData + "modelo").Value
                            }).ToList();

                foreach (var lista in list)
                {
                    Modelos.Add(new modelos(sub.claveMarca, sub.claveSubMarcaAuto, sub.descripcion, Int32.Parse(lista.modelo), sub.tipoVehiculo));
                    sal += sub.claveMarca + "|" +sub.claveSubMarcaAuto + "|" + sub.descripcion +"|"+ lista.modelo+"|" + sub.tipoVehiculo +  "<br>";
                    i++;
                }
                j++;


            }
            //Label2.Text = sal;
            CrearTxt(sal, "Zurich_Modelos");
            return true;
        }catch(Exception exc)
        {
            errores.Add(exc.Message + " |getModelos| <br>");
            return false;
        }
    }

    public Boolean getDescripciones()
    {
        string salida = "";
        string sal = "";
        int i = 0;
        int j = 0;

        try
        {
            foreach (modelos mod in Modelos)
            {
                salida = ApiConsume("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservices.zurich.com/\"><soapenv:Header><wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\"><UsernameToken xmlns=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\"><Username>"+this.usr+"</Username><Password>"+this.usr+"</Password></UsernameToken></wsse:Security></soapenv:Header><soapenv:Body><web:reqClaveZurichAuto><web:numRequest>14</web:numRequest><web:catalogo>CAUTO</web:catalogo><web:tipoVehiculo>"+mod.tipoVehiculo+"</web:tipoVehiculo><web:marca>"+mod.claveMarca+"</web:marca><web:submarca>"+mod.claveSubMarca+"</web:submarca><web:modelo>"+mod.modelo+"</web:modelo><web:numRelacion>8701022</web:numRelacion><web:usuario>"+this.usr+"</web:usuario><web:agente>"+this.agente+"</web:agente></web:reqClaveZurichAuto></soapenv:Body></soapenv:Envelope>",this.claves);
                XDocument doc = XDocument.Parse(salida);
                XNamespace metaData = "http://webservices.zurich.com/";
                var list = (from d in doc.Descendants(metaData + "claveZurich")
                            select new
                            {
                                clave = d.Element(metaData + "clave").Value,
                                descripcion = d.Element(metaData + "descripcion").Value
                            }).ToList();
                foreach (var lista in list)
                {
                    sal += mod.claveMarca + "|" + mod.claveSubMarca + "|" + lista.clave + "|" + lista.descripcion + "|" + mod.modelo + "|" + mod.tipoVehiculo + "<br>";
                    j++;
                    System.Diagnostics.Debug.WriteLine(mod.claveMarca + "|" + mod.claveSubMarca + "|" + lista.clave + "|" + lista.descripcion + "|" + mod.modelo + "|" + mod.tipoVehiculo + "[" + i + "]" + "[" + j + "]");
                }
                i++;
            }
            //Label2.Text = salida;
            CrearTxt(sal, "Zurich_descripciones");
            return true;
        }
        catch (Exception exc)
        {
            errores.Add(exc.Message + " |getDescripciones| <br>");
            return false;
        }
    }


    public String ApiConsume(String Content , string name)
    {
        try
        {
            //Creacion del objeto RestSharp
            var client = new RestClient(name);

            var request = new RestRequest(Method.POST);
            //Declaracion de los headers del objeto declaracion 
            //Le decimos que acepte respuestas de todo tipo sin importar el formato 
            request.AddHeader("Accept", "*/*");
            //Declaramos que el tipo de cuerpo va a ser en xml
            request.AddHeader("Content-Type", "application/xml;");
            //Añadimos el cuerpo a el request
            request.AddParameter("application/xml", Content, ParameterType.RequestBody);
            //Ejecutamos el request y cuardamos la respuesta en un objeto de tipo IRestResponse que es parte de RestSharp
            IRestResponse response = client.Execute(request);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                this.errores.Add(response.Content +response.ResponseStatus + response.StatusCode+ "error en la peticion");
                return "fail";
            }
            return response.Content;
        }
        catch(Exception exc)
        {
            this.errores.Add(exc.Message + " |ApiConsume|<br>");
            return "fail";
        }
        //Label2.Text = response.Content;

    }

    private Boolean Almacenar()
    {
        try
        {
            //se crea la conexion a la base de datos , los parametros tienen que cambiarse
            using (var conn = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=ZurichCatalogos;user id = servici1; password = Master@011"))
            {
                //se accede y se ejecuta a el stop procedure y se le asigna un timeout de 0 para que no interrumpa la conexion
                using (var command = new SqlCommand("[SP.ZurichCatalogos]", conn) { CommandType = CommandType.StoredProcedure })
                {
                    command.CommandTimeout = 0;
                    conn.Open();
                    command.ExecuteNonQuery();
                    conn.Close();


                    return true;
                }
            }
        }
        catch (Exception excep)
        {
            this.errores.Add(excep.Message + " |Almacenar| \n");
            return false;
        }
    }

    //public Body deserializarMarcas(string name)
    //{
    //    Body catalogo = null;
    //    XmlSerializer serial = new XmlSerializer(typeof(Body));
    //    try
    //    {
    //        using (var reader = new StringReader(name.ToString()))
    //        {
    //            catalogo = (Body)serial.Deserialize(reader);
    //            return catalogo;
    //        }
    //    }
    //    catch (Exception exv)
    //    {
    //        this.errores.Add(exv.Message + " |deserializacion| \n");
    //        return catalogo;
    //    }
    //}
    private void CrearTxt(String salida, String name)
    {
        //inicia el intento de crear el archivo
        try
        {
            //genera el archivo dentro del escritorio con el nombre y la cadena dada
            FileStream file = File.Create(this.path + name + ".txt");
            Byte[] info = new UTF8Encoding(true).GetBytes(salida);
            file.Write(info, 0, info.Length);
            file.Close();
        }
        catch (Exception excep)
        {
            this.errores.Add(excep.Message + "|" + excep.InnerException + "|" + excep.StackTrace);
        }
    }

    public Boolean tipoCarga()
    {
        if (usos.Count == 0)
            return false;

        string salida = "";
        string sal = "";
        try
        {
            foreach(usos uso in usos)
            {
                salida = ApiConsume("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservices.zurich.com/\" ><soapenv:Header><wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" ><UsernameToken xmlns=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" ><Username>" + this.usr + "</Username><Password>" + this.usr + "</Password></UsernameToken></wsse:Security></soapenv:Header><soapenv:Body>      <web:CatTipoCargaNumeroRelacionReq>         <web:numRequest>2</web:numRequest>         <web:nombreCatalogo>TPCAR</web:nombreCatalogo>         <web:numRelacion>8701022</web:numRelacion><web:tipoVehiculo>1</web:tipoVehiculo><web:tipoUso>"+uso.cveTipoUso+"</web:tipoUso><web:usuario>" + this.usr + "</web:usuario><web:agente>" + this.agente + "</web:agente></web:CatTipoCargaNumeroRelacionReq></soapenv:Body></soapenv:Envelope>",this.catalogos);
                XDocument doc = XDocument.Parse(salida);
                XNamespace metaData = "http://webservices.zurich.com/";

                var list = (from d in doc.Descendants(metaData + "elementosCatTipoCargaNumsRel")
                            select new
                            {
                                idTipoCarga = d.Element(metaData + "idTipoCarga").Value,
                                descElemento = d.Element(metaData + "descElemento").Value
                            }).ToList();

                foreach (var lista in list)
                {
                    sal += uso.cveTipoUso +"|"+ lista.idTipoCarga + "|" + lista.descElemento +"<br>";
                }
            }

            CrearTxt(sal, "Zurich_Cargas");
            return true;
        } catch (Exception exc)
        {
            errores.Add(exc.Message + " |tipoCarga| <br>");
            return false;
        }
    }

    public Boolean tipoUso()
    {
        string salida = "";
        string sal = "";
        try
        {
            salida += ApiConsume("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservices.zurich.com/\" ><soapenv:Header><wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" ><UsernameToken xmlns=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" ><Username>" + this.usr + "</Username><Password>" + this.usr + "</Password></UsernameToken></wsse:Security></soapenv:Header><soapenv:Body><web:CatTipoUsoReq><web:numRequest>16</web:numRequest>        <web:catalogo>USO</web:catalogo>         <web:tipoVehiculo>1</web:tipoVehiculo>        <web:numRelacion>8701022</web:numRelacion><web:usuario>" + this.usr + "</web:usuario><web:agente>" + this.agente + "</web:agente></web:CatTipoUsoReq></soapenv:Body></soapenv:Envelope>",this.catalogos);
            XDocument doc = XDocument.Parse(salida);
            XNamespace metaData = "http://webservices.zurich.com/";

            var list = (from d in doc.Descendants(metaData + "tipoUso")
                        select new
                        {
                            cveTipoUso = d.Element(metaData + "cveTipoUso").Value,
                            descripcion = d.Element(metaData + "descripcion").Value
                        }).ToList();

            foreach (var lista in list)
            {
                usos.Add(new usos(Int32.Parse(lista.cveTipoUso), lista.descripcion));
                sal +=  lista.cveTipoUso + "|" + lista.descripcion + "<br>";
            }
           // Label2.Text = salida;
            CrearTxt(sal, "Zurich_Usos");
            return true;
        }
        catch (Exception exc)
        {
            errores.Add(exc.Message + " |tipoCarga| <br>");
            return false;
        }
    }

    public Boolean tipoVehiculo()
    {
        string salida = "";
        string sal = "";
        try
        {
            salida = ApiConsume("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservices.zurich.com/\" ><soapenv:Header><wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" ><UsernameToken xmlns=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" ><Username>" + this.usr + "</Username><Password>" + this.usr + "</Password></UsernameToken></wsse:Security></soapenv:Header><soapenv:Body>      <web:tipoVehiculoRequest>         <web:numRequest>10</web:numRequest>         <web:catalogo>VEHCL</web:catalogo>         <web:numRelacion>8701022</web:numRelacion>         <web:usuario>"+this.usr+"</web:usuario>         <web:agente>"+this.agente+"</web:agente>      </web:tipoVehiculoRequest>   </soapenv:Body></soapenv:Envelope>",this.catalogos);
            XDocument doc = XDocument.Parse(salida);
            XNamespace metaData = "http://webservices.zurich.com/";

            var list = (from d in doc.Descendants(metaData + "tipoVehiculo")
                        select new
                        {
                            cveTipo = d.Element(metaData + "cveTipo").Value,
                            descripcion = d.Element(metaData + "descripcion").Value
                        }).ToList();

            foreach (var lista in list)
            {
                sal += lista.cveTipo + "|" + lista.descripcion + "<br>";
            }
            // Label2.Text = salida;
            CrearTxt(sal, "Zurich_Tipos");
            return true;
        }
        catch (Exception exc)
        {
            errores.Add(exc.Message + " |tipoCarga| <br>");
            return false;
        }
    }


    public Boolean tipoValor()
    {
        string salida = "";
        string sal = "";
        try
        {
            for(int i = 1999;i<DateTime.Now.Year+1;i++)
            {
                salida = ApiConsume("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservices.zurich.com/\" ><soapenv:Header><wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" ><UsernameToken xmlns=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" ><Username>" + this.usr + "</Username><Password>" + this.usr + "</Password></UsernameToken></wsse:Security></soapenv:Header><soapenv:Body><web:reqCatTiposValorAuto>         <web:numRequest>15</web:numRequest>         <web:catalogo>TIVAL</web:catalogo>         <web:producto>0</web:producto>         <web:numRelacion>8701022</web:numRelacion>         <web:usuario>"+this.usr+"</web:usuario>         <web:agente>"+this.agente+"</web:agente>         <web:tipoVehiculo>1</web:tipoVehiculo>         <web:modelo>"+i.ToString()+"</web:modelo>      </web:reqCatTiposValorAuto>   </soapenv:Body></soapenv:Envelope>",this.catalogos);
                XDocument doc = XDocument.Parse(salida);
                XNamespace metaData = "http://webservices.zurich.com/";

                var list = (from d in doc.Descendants(metaData + "resCatTiposValorAuto")
                            select new
                            {
                                status = d.Element(metaData + "status").Value,
                                mensaje = d.Element(metaData + "mensaje").Value
                            }).ToList();

                foreach (var lista in list)
                {
                        sal += lista.status + "|" + lista.mensaje + "|" +i.ToString()+"<br>";
                }
            }
            
            // Label2.Text = salida;
            CrearTxt(sal, "Zurich_Tipo_Valor");
            return true;
        }
        catch (Exception exc)
        {
            errores.Add(exc.Message + " |tipoCarga| <br>");
            return false;
        }
    }


}

public class marca
{
    public int claveMarca { get; set; }
    public string descripcion { get; set; }

    public string tipoVehiculo { get; set; }


    public marca(int clave , String desc , string tipo)
    {
        claveMarca = clave;
        descripcion = desc;
        tipoVehiculo = tipo;
    }

}

public class submarca
{
    public int claveSubMarcaAuto { get; set; }
    public string descripcion { get; set; }

    public int claveMarca { get; set; }

    public string tipoVehiculo { get; set; }


    public submarca(int clave, String desc , int claveMarca , string tipo)
    {
        claveSubMarcaAuto = clave;
        descripcion = desc;
        this.claveMarca = claveMarca;
        tipoVehiculo = tipo;
    }
}

public class usos
{
    public int cveTipoUso { get; set; }
    public string descripcion { get; set; }

    public usos(int clave, String desc)
    {
        cveTipoUso = clave;
        descripcion = desc;
    }
}


public class modelos
{
    public int claveMarca { get; set; }
    public int claveSubMarca { get; set; }
    public string descSub { get; set; }

    public int modelo { get; set; }
    public string tipoVehiculo { get; set; }

    public modelos(int claveMarca , int claveSubMarca , String descSub, int modelo , String tipo)
    {
        this.claveMarca = claveMarca;
        this.claveSubMarca = claveSubMarca;
        this.descSub = descSub;
        this.modelo = modelo;
        this.tipoVehiculo = tipo;

    }
}

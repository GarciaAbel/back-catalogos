﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
            //It is prefereable that you kick off the task on the previous page and then redirect to this page
            //in order to prevent duplicate submission of tasks
            StartLongRunningTask();

    }

    /// <summary>
    /// Just setting server vairables here, this is where you would kick off the long running task.
    /// </summary>
    private void StartLongRunningTask()
    {
        this.Application["MaxIndex"] = "20";
        this.Application["Counter"] = "1";
    }

    /// <summary>
    /// This triggers the polling to start and constantly check for the task status
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnLoop_Click(object sender, EventArgs e)
    {
        lblDescription.Visible = imgProgress.Visible = true;
        mainTimer.Enabled = true;
    }

    /// <summary>
    /// This methods gets executed at intervals specified within the timer control,
    /// in this example its set to 500ms
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void mainTimer_Tick(object sender, EventArgs e)
    {
        //This example uses the Application object to persist the status of the task
        int maxIndex = Convert.ToInt32(this.Application["MaxIndex"]);
        int counter = Convert.ToInt32(this.Application["Counter"]);

        if (counter >= maxIndex)
        {
            this.imgProgress.Visible = false;
            this.mainTimer.Enabled = false;
            lblDescription.Text = "Completed processing all records.";
        }
        else
        {
            this.lblDescription.Text = "Processing " + counter + " of " + maxIndex + " records";
            this.Application["Counter"] = ++counter;
        }

    }

}
   


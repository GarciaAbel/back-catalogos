﻿//Abel Garcia

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Serialization;

public partial class _Default : System.Web.UI.Page
{

    private String usr = "GMAC0106";
    private String pass = "MXMIFNLKYZAQH5PRPHRLLA==";
    private String token = "94403460-fb76-4e9d-b1d4-244fa7c52c55";
    private String path = @"C:\Users\ultron\Desktop\";
    private List<String> errores;
    private List<Marca> marcas;

    protected void Page_Load(object sender, EventArgs e)
    {

    } 

    protected void Button1_Click(object sender, EventArgs e)
    {
        String error = "";
        int intentos = 0;

        errores = new List<String>();
        marcas = new List<Marca>();

        do
        {
            marcas.Clear();


            if (doGeneracion())
            {
                error= "Se a completado con exito";
                break;
            }
                
            else
            {
                foreach (String str in this.errores)
                    error += str + "|Intento: "+intentos+"|<br>";

                
                intentos++;
                this.errores.Clear();
            }

        } while (intentos < 5);

        Label1.Text = error;
    }

    private Boolean doGeneracion()
    {
       // string salida = "";
        try
        {
            MapfreWS.CatalogosAutosSoapClient map = new MapfreWS.CatalogosAutosSoapClient();
            if (getMarcas(map) && errores.Count == 0)
            {
                if (getModelos(map) && errores.Count == 0 && marcas.Count != 0)
                {
                    Almacenar();
                }
            }
        }
        catch (Exception exc)
        {
            this.errores.Add(exc.Message + " |Inicio| \n");
        }

        if (this.errores.Count != 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    //private Boolean find(String name, String clave,int año)
    //{
    //    int flag = 0;

    //    try
    //    {
    //        foreach (Marca marca in this.marcas)
    //        {
    //            if (name == marca.nombre_marca)
    //            {
    //                flag = 1;
    //                marca.setAño(año.ToString());
    //                break;
    //            }
    //        }
    //        if (flag == 0)
    //        {
    //            return true;
    //        }
    //        else
    //            return false;
    //    } catch (Exception exc)
    //    {
    //        this.errores.Add(exc.Message + " |Funcion Find| \n");
    //        return false;
    //     }
        
    //}

    private Boolean getModelos(MapfreWS.CatalogosAutosSoapClient map)
    {
        string salida = "";
        data dat = null;
        try
        {
            foreach(Marca marca in this.marcas)
            { 
                    dat = deserializarMarcas(map.WS_TW_Modelos("<xml><data><valor cod_ramo='401' cod_marca='"+marca.clave_marca+"' cod_tip_vehi='"+marca.tipo+"' cod_zona_agt='99' anio_fabrica='"+marca.años+"' id_negocio='LOCALES' /></data></xml>", this.token));
                    foreach (lista list in dat.listas)
                    {
                        salida += marca.clave_marca+"|"+list.NOM_MODELO + "|" + list.COD_MODELO + "|" + marca.años + "|" + marca.tipo +   "<br>";
                        //System.Diagnostics.Debug.WriteLine(marca.clave_marca + "|" + list.NOM_MODELO + "|" + list.COD_MODELO + "|" + marca.años + "|" + marca.tipo + "<br>");

                    }

            }
            CrearTxt(salida, "Mapfre_modelos");
            //Label2.Text = salida;
            return true;
        }
        catch (Exception exc)
        {
            this.errores.Add(exc.Message + "|getModelos| \n");
            return false;
        }
    }

    private Boolean getMarcas(MapfreWS.CatalogosAutosSoapClient map)
    {
        string[] tipos = { "1", "2", "3", "4", "6", "7", "8", "11", "12" };
        string salida = "";
        try
        {
            foreach(string str in tipos)
            {
                for(int i = 1900; i < DateTime.Now.Year; i++)
                {
                    data dat = deserializarMarcas(map.WS_TW_Marcas("<xml><data><valor cod_ramo='401' cod_zona_agt='99' cod_tip_vehi='"+str+"' anio_fabrica='" + i.ToString() + "' id_negocio='LOCALES' /></data></xml>", this.token));
                    foreach (lista lis in dat.listas)
                    {
    
                     this.marcas.Add(new Marca(lis.COD_MARCA, lis.NOM_MARCA, i.ToString(),str));
                      salida += lis.COD_MARCA + "|" + lis.NOM_MARCA + "|" + i.ToString() + "|" + str + "<br>";

                    }
                }
            }
            CrearTxt(salida, "Mapfre_marcas");
            return true;
        }
        catch (Exception exc)
        {
            this.errores.Add(exc.Message + " |getMarcas|\n");
            return false;
        }
    }

    public data deserializarMarcas(System.Xml.XmlNode name)
    {
        data catalogo = null;
        XmlSerializer serial = new XmlSerializer(typeof(data));
        MemoryStream stm = new MemoryStream();
        StreamWriter stw = new StreamWriter(stm);

        try
        {
            stw.Write(name.OuterXml);
            stw.Flush();

            stm.Position = 0;

            catalogo = (serial.Deserialize(stm) as data);
           
        }
        catch (Exception exv)
        {
            this.errores.Add(exv.Message+"|deserializacion|\n");
        }

        serial = null;
        stm = null;
        stw = null;
        return catalogo;
        
    }

    private void CrearTxt(String salida, String name)
    {
        //inicia el intento de crear el archivo
        try
        {
            //genera el archivo dentro del escritorio con el nombre y la cadena dada
            FileStream file = File.Create(this.path + name + ".txt");
            Byte[] info = new UTF8Encoding(true).GetBytes(salida);
            file.Write(info, 0, info.Length);
            file.Close();
        }
        catch (Exception excep)
        {
            this.errores.Add(excep.Message + "|" + excep.InnerException + "|" + excep.StackTrace);
        }
    }
    private String Almacenar()
    {
        try
        {
            //se crea la conexion a la base de datos , los parametros tienen que cambiarse
            using (var conn = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=Mapfre;user id = servici1; password = Master@011"))
            {
                //se accede y se ejecuta a el stop procedure y se le asigna un timeout de 0 para que no interrumpa la conexion
                using (var command = new SqlCommand("[SP.MapfreCatalogos]", conn) { CommandType = CommandType.StoredProcedure })
                {
                    command.CommandTimeout = 0;
                    conn.Open();
                    command.ExecuteNonQuery();
                    conn.Close();


                    return "ok";
                }
            }
        }
        catch (Exception excep)
        {
            errores.Add(excep.Message + "|almacenar| \n");
            return excep.Message;
        }
    }

}

[Serializable()]
public class lista
{
    [System.Xml.Serialization.XmlElement("COD_MARCA")]
    public string COD_MARCA { get; set; }

    [System.Xml.Serialization.XmlElement("NOM_MARCA")]
    public string NOM_MARCA { get; set; }

    [System.Xml.Serialization.XmlElement("COD_MODELO")]
    public string COD_MODELO { get; set; }

    [System.Xml.Serialization.XmlElement("NOM_MODELO")]
    public string NOM_MODELO { get; set; }
}

[Serializable()]
[System.Xml.Serialization.XmlRoot("xml")]
public class data
{
    [XmlArray("data")]
    [XmlArrayItem("lista", typeof(lista))]
    public lista[] listas { get; set; }
}

public class Marca
{
    public String clave_marca { set; get; }
    public String nombre_marca { set; get; }
    public String años { set; get; }

    public String tipo { set; get; }
    public Marca(String clave, string nombre,String año,String tipo)
    {
        clave_marca = clave;
        nombre_marca = nombre;
        años = año;
        this.tipo = tipo;
    }
}
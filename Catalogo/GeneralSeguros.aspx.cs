﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;

public partial class _Default : System.Web.UI.Page
{
    private String path = @"C:\Users\ultron\Desktop\";
    private List<Marcas> marcas;
    private List<SubMarcas> submarcas;
    private List<SubMarcas> submod;
    private List<String> errores;
    private List<string> track;


    private string usr = "ATC0";
    private string pass = "2r2kGdeUA0";
    private string token = "";

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public Boolean insertar(string name)
    {

        try
        {
            if(name == "GS_subMarcas" && marcas.Count == 0)
            {
                string tex = File.ReadAllText(this.path + "GS_Marcas" + ".txt").Replace("<br>", "|");
                string[] sal = tex.Split('|');

                for(int i = 0; i<sal.Length-1;i++)
                {
                    marcas.Add(new Marcas(Int32.Parse(sal[i]), sal[i + 1]));
                    i++;
                }

                return true;
            }

            else if (name == "GS_Versiones" && submarcas.Count == 0)
            {
                List<int> subaños = new List<int>();

                string tex = File.ReadAllText(this.path + "GS_subMarcas" + ".txt").Replace("<br>", "|");
                string[] sal = tex.Split('|');

                for (int i = 0; i < sal.Length - 1;)
                {
                    this.submarcas.Add(new SubMarcas(Int32.Parse(sal[i]),Int32.Parse(sal[i+1]),sal[i+2],Int32.Parse(sal[i+3]),Int32.Parse(sal[i+4])));
                    i += 5;
                }
                return true;
            }

            return false;
        }
        catch (Exception exc)
        {
            errores.Add(exc.Message + " | " + name + " |");
            return false;
        }
    }


    public Boolean verificar(string name)
    {
        try
        {
            if (File.Exists(this.path + name + ".txt"))
            {

                track.Add(name);
                return true;
            }
            else
            {
                insertar(name);
                return false;
            }
        }
        catch (Exception exc)
        {
            errores.Add(exc.Message + " |Verificar|<br>");
            return false;
        }
    }


    public Boolean getMarcas(GSWS.CatalogoAutosWSClient gs,GSWS.request request)
    {
        string salida = "";
        try
        {
            var marca = gs.wsListarMarcas(request);
            GSWS.marca[] marcasWS = null;

            if (marca.exito)
            {
                marcasWS = marca.marcas;
            }
            else
            {
                errores.Add(marca.mensaje + " |error WS getMarcas| ");
                return false;
            }


            foreach (GSWS.marca marc in marcasWS)
            {
                marcas.Add(new Marcas(marc.id, marc.nombre));
                salida += marc.id + "|" + marc.nombre + "<br>";
            }

            CrearTxt(salida, "GS_Marcas");
            return true;
        }
        catch (Exception exc)
        {
            errores.Add(exc.Message + " |getMarcas|");
            return false;
        }
    }

    public Boolean getsubmarcas(GSWS.CatalogoAutosWSClient gs)
    {
        string salida = "";
        try
        {
            GSWS.listarSubMarcasRequest submarcasrequest = new GSWS.listarSubMarcasRequest();
            submarcasrequest.token = this.token;
          
            foreach (Marcas marc in marcas)
            {
                submarcasrequest.idMarca = marc.id;
                GSWS.listarSubMarcasResponse sub = gs.wsListarSubMarcas(submarcasrequest);
                GSWS.submarca[] submarcas = sub.submarcas;

                foreach(GSWS.submarca subM  in submarcas)
                {
                    this.submarcas.Add(new SubMarcas(marc.id,subM.id,subM.nombre,subM.idSegmento,0));
                    //salida += marc.id + "|" + subM.id +"|"+ subM.nombre + "|" + subM.idSegmento + "<br>";
                }
            }

            //Label2.Text = salida;

            //CrearTxt(salida, "GS_sub_marcas");
            return true;
        }
        catch (Exception exc)
        {
            errores.Add(exc.Message + " |getSubMarcas|");
            return false;
        }  
    }


    public Boolean getModelos(GSWS.CatalogoAutosWSClient gs)
    {
        string salida = "";
        string años = "";
        try
        {

            GSWS.listarModelosRequest mod = new GSWS.listarModelosRequest();
            mod.token = this.token;

            foreach(SubMarcas sub in submarcas)
            {
                mod.idSubmarca = sub.id_submarca;
                GSWS.listarModelosResponse resp=gs.wsListarModelos(mod);
                int?[] modelo = resp.modelos;
                if(modelo!=null)
                {
                    foreach (int entero in modelo)
                    {
                        submod.Add(new SubMarcas(sub.id_marca, sub.id_submarca, sub.nombre_submarca, sub.segmento, entero));
                        salida += sub.id_marca + "|" + sub.id_submarca + "|" + sub.nombre_submarca + "|" + sub.segmento + "|" + entero + "<br>";
                    }
                }
            }     
            CrearTxt(salida, "GS_subMarcas");
            return true;

        }
        catch(Exception exc)
        {
            errores.Add(exc.Message + " |getModelos |");
            return false;
        }
    }

    public Boolean getVersiones(GSWS.CatalogoAutosWSClient gs)
    {
        string salida = "";

        try
        {
            GSWS.listarVersionesRequest version = new GSWS.listarVersionesRequest();
            version.token = this.token;

            foreach(SubMarcas sub in submod)
            {
                version.idSubmarca = sub.id_submarca;
                version.modelo = sub.años;
                GSWS.version[] versions = gs.wsListarVersiones(version).versiones;
                if(versions!= null)
                {
                    foreach (GSWS.version versionT in versions)
                    {
                        salida += sub.id_marca + "|" + sub.id_submarca + "|" + sub.años + "|" + versionT.descripcion + "|" + versionT.amis + "<br>";
                    }
                }
            }
            Label2.Text = salida;
            CrearTxt(salida, "GS_Versiones");
            return true;
        }
        catch(Exception exc)
        {
            errores.Add(exc.Message + " |GetVersiones|");
            return false;
        }
    }

    public void delete()
    {
        foreach (string str in track)
        {
            File.Delete(path + str + ".txt");
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        marcas = new List<Marcas>();
        submarcas = new List<SubMarcas>();
        submod = new List<SubMarcas>();
        errores = new List<String>();
        track = new List<string>();


    string error = "";
        string[] names = {"GS_Versiones", "GS_subMarcas", "GS_Marcas"};
        try
        {
            GSToken.AutenticacionWSClient token = new GSToken.AutenticacionWSClient();
            GSToken.requestAutenticacion requestTok = new GSToken.requestAutenticacion();

            requestTok.usuario = this.usr;
            requestTok.password = this.pass;

            this.token =  token.obtenerToken(requestTok).token;

            GSWS.CatalogoAutosWSClient gs = new GSWS.CatalogoAutosWSClient();

            GSWS.request request = new GSWS.request();
            request.token = this.token;

            if (errores.Count == 0 && (verificar(names[2])||getMarcas(gs,request)))
            {
                if(errores.Count == 0 && (verificar(names[1])||getsubmarcas(gs)))
                {
                    if (errores.Count == 0 && (verificar(names[1]) || getModelos(gs)))
                    {
                        if (errores.Count == 0 && (verificar(names[0]) || getVersiones(gs)))
                        {
                            if (track.Count == 4)
                            {
                                delete();
                                Button1_Click(sender,e);
                                //return false;
                            }
                            else
                            {
                                Almacenar();
                            }
                        }
                    }
                }
            }
        }
        catch(Exception exc)
        {
            errores.Add(exc.Message + " |Inicio|<br> ");
            //return false;
        }

        if (errores.Count != 0)
            foreach (string str in errores)
                error = str + "<br>";

        else
            error = "se a completado con exito";


        Label1.Text = error;

    }

    private void CrearTxt(String salida, String name)
    {
        //inicia el intento de crear el archivo
        try
        {
            //genera el archivo dentro del escritorio con el nombre y la cadena dada
            FileStream file = File.Create(this.path + name + ".txt");
            Byte[] info = new UTF8Encoding(true).GetBytes(salida);
            file.Write(info, 0, info.Length);
            file.Close();
        }
        catch (Exception excep)
        {
            this.errores.Add(excep.Message + "|" + excep.InnerException + "|" + excep.StackTrace);
        }
    }


    private Boolean Almacenar()
    {
        try
        {
            //se crea la conexion a la base de datos , los parametros tienen que cambiarse
            using (var conn = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=GSCatalogos;user id = servici1; password = Master@011"))
            {
                //se accede y se ejecuta a el stop procedure y se le asigna un timeout de 0 para que no interrumpa la conexion
                using (var command = new SqlCommand("[SP.GS_Catalogos]", conn) { CommandType = CommandType.StoredProcedure })
                {
                    command.CommandTimeout = 0;
                    conn.Open();
                    command.ExecuteNonQuery();
                    conn.Close();


                    return true;
                }
            }
        }
        catch (Exception excep)
        {
            this.errores.Add(excep.Message + " |Almacenar| \n");
            return false;
        }
    }

}


public class Marcas
{
    public int id { set; get; }
    public string nombre_marca { set; get; }
    public Marcas(int id, string nombre)
    {
        this.id = id;
        nombre_marca = nombre;
    }
}
public class SubMarcas
{
    public int id_marca { set; get; }
    public int id_submarca { set; get; }
    public string nombre_submarca { set; get; }
    public int segmento  { set; get; }
    public int años { set; get; }
    public SubMarcas(int id, int idsub , string nombresub, int segmento,int años)
    {
        this.id_marca = id;
        id_submarca = idsub;
        nombre_submarca = nombresub;
        this.segmento = segmento;
        this.años = años;
    }
}
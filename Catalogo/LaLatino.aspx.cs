﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LatinoWS;

public partial class _Default : System.Web.UI.Page
{

    protected static string content;
    protected static bool inProcess = false;
    protected static bool processComplete = false;
    protected static string processCompleteMsg = "Finished Processing All Records.";
    private static DateTime time;
    //private delegate void SafeCallDelegate(string text);
    private String path = @"C:\Users\ultron\Desktop\";
   
    private static Semaphore _pool;

    private long usr = 10041;
    private string pass = "CD56JH34ED23MRVC92";
    private string usrlog = "9257";
    private string usrpss = "L4t1nO9257";

    private List<marcasLat> marcas ;
    private List<SubMarca> submarcas;
    private List<String> descripciones;
    private List<String> errores;
    private List<string> track;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        marcas = new List<marcasLat>();
        submarcas = new List<SubMarca>();
        descripciones = new List<string>();
        errores = new List<String>();
        track = new List<string>();

        Timer1.Enabled = true;
        content = "";
        Button1.Enabled = false;
        time = DateTime.Now;
        marcas.Clear();
        submarcas.Clear();
        descripciones.Clear();

        if(_pool!=null)
            _pool.Dispose();

        Thread work = new Thread(() => doThings());
        work.Start();


    }

    //-------------------------------------------Main Function-------------------------------------------

    public void doThings()
    {
        try
        {
            inProcess = true;
            LatinoWS.CotizadorLatinoClient latino = new LatinoWS.CotizadorLatinoClient();
            LatinoWS.DatosRequeridos dat = new LatinoWS.DatosRequeridos();

            LatinoWS.Credencial cred = new LatinoWS.Credencial();
            cred.IdApp = this.usr;
            cred.PassApp = this.pass;
            cred.ClaveUsuario = this.usrlog;
            cred.Password = this.usrpss;
            
            LatinoWS.Auto aut = new LatinoWS.Auto();
            aut.ClaveProducto = "1";
            aut.Tarifa = "1704";
            aut.TipoVehiculo = "AU";
            aut.ClavePerfil = "36013";
            aut.ClaveMarca = "";
            aut.ClaveSubMarca = "";
            aut.ClaveVehiculo = "";
            aut.NumeroSerieAuto = "";
            aut.SerieValida = false;
            aut.NumeroPlacas = "";
            aut.NumeroMotor = "";

            LatinoWS.CaracteristicasCotizacion cot= new LatinoWS.CaracteristicasCotizacion();
            cot.ClaveEstado = "";
            cot.ClavePaquete = "";
            cot.ClaveVigencia = "";
            cot.ClaveServicio = "";
            cot.ClaveDescuento = "";
            cot.ClaveAgente = "";
            cot.ClaveFormaPago = "";
            dat.datosAuto = aut;
            dat.credenciales = cred;
            dat.caracteristicasCotizacion = cot;
            if (errores.Count == 0 && getCatalogos(latino, dat))
            {
            
                if(errores.Count == 0 && getSubMarcas(latino, dat))
                {
                    if(errores.Count == 0 &&  getDescripcion(latino , dat))
                    {
                        if (errores.Count == 0 && getMarcas(latino, dat))
                        {

                        }
                        Almacenar();
                    }
                }
            }

            processComplete = true;
            content = processCompleteMsg + System.Environment.NewLine + content;

        }
        catch(Exception exc)
        {
            content = exc.Message + " Main Function " + System.Environment.NewLine;
            errores.Add(exc.Message + " |Main Function|");
        }
    }

    private Boolean getCatalogos(CotizadorLatinoClient latino, DatosRequeridos dat)
    {
        try
        {

            string estados = "";
            string paquetes = "";
            string forma = "";
            string servicio = "";
            string tipo = "";

            LatinoWS.ListCatalogos catalogos = latino.ObtenerCatalogos(dat);

            foreach(LatinoWS.Estado estado in catalogos.Estados)
            {
                estados += estado.Clave + "|" + estado.Descripcion+"<br>"; 
            }

            foreach (LatinoWS.Paquete paquete in catalogos.Paquetes)
            {
                paquetes += paquete.Clave + "|" + paquete.Descripcion + "<br>";
            }

            foreach (LatinoWS.FormaPago fp in catalogos.FormasPagos)
            {
                forma += fp.Clave + "|" + fp.Descripcion + "<br>";
            }

            foreach (LatinoWS.Servicio servicios in catalogos.Servicios)
            {
                servicio += servicios.Clave + "|" + servicios.Descripcion + "<br>";
            }

            foreach (LatinoWS.TipoVigencia tipos in catalogos.TiposVigencias)
            {
                tipo += tipos.Clave + "|" + tipos.Descripcion + "<br>";
            }

            CrearTxt(estados,"LatinoEstados");
            CrearTxt(paquetes,"LatinoPaquetes");
            CrearTxt(forma , "LatinoForma");
            CrearTxt(servicio,"LatinoServicio");
            CrearTxt(tipo,"LatinoTipos");
            return true;
        }
        catch (Exception exc)
        {
            content = exc.Message + " Main Function " + System.Environment.NewLine;
            errores.Add(exc.Message + " |Main Function|");
            return false;
        }

    }



    //-----------------------------------------Threads Managers-----------------------------------------

    public Boolean getMarcas(LatinoWS.CotizadorLatinoClient latino, LatinoWS.DatosRequeridos dat)
    {
        try
        {
            string sal = "";
            _pool = new Semaphore(1, 50);
            Thread[] threads = new Thread[50];

            for (int i = 1997; i<DateTime.Now.Year+2;)
            {
                for(int j = 0; j<50;j++)
                {
                    if(i<DateTime.Now.Year+1)
                    {
                        threads[j] = new Thread(() => getMC(latino,dat, i.ToString()));
                        threads[j].Start();
                        Thread.Sleep(50);
                    }
                    i++;
                }
                foreach (Thread th in threads)
                {
                    if (th != null && th.IsAlive)
                        th.Join();
                }

            }

            foreach(marcasLat lat in marcas)
            {
                sal += lat.clave + "|" + lat.descripcion + "|" + lat.año + "<br>";
            }

            CrearTxt(sal,"Latino_marcas");
            _pool.Dispose();
            return true;
        }catch(Exception exc)
        {
            errores.Add(exc.Message + " |Main Function|");
            return false;
        }
    }

    private bool getSubMarcas(CotizadorLatinoClient latino, DatosRequeridos dat)
    {
        try
        {
            string sal = "";
            _pool = new Semaphore(1, 100);
            Thread[] threads = new Thread[100];

            for (int i = 0; i < marcas.Count();)
            {
                for (int j = 0; j < 100; j++)
                {
                    if (i < marcas.Count())
                    {
                        threads[j] = new Thread(() => getSM(latino, dat, marcas[i]));
                        threads[j].Start();
                        Thread.Sleep(50);
                    }
                    i++;
                }
                foreach (Thread th in threads)
                {
                    if (th != null && th.IsAlive)
                        th.Join();
                }
            }

            foreach (SubMarca lat in submarcas)
            {
                sal += lat.clv_marca + "|" + lat.clv_submarca + "|" + lat.descripcion + "|" + lat.año + "<br>";
            }

            CrearTxt(sal, "Latino_Submarcas");

            _pool.Dispose();
            return true;
        }catch(Exception exc)
        {
            errores.Add(exc.Message + " |Main Function|");
            return false;
        }


    }

    public Boolean getDescripcion(CotizadorLatinoClient latino, DatosRequeridos dat)
    {
        try
        {
            string sal = "";
            _pool = new Semaphore(1, 200);
            Thread[] threads = new Thread[200];

            for(int i = 0; i<submarcas.Count;)
            {
                for (int j = 0; j < 200; j++)
                {
                    if (i < submarcas.Count())
                    {
                        threads[j] = new Thread(() => getDS(latino, dat , submarcas[i]));
                        threads[j].Start();
                        Thread.Sleep(50);
                    }
                    i++;
                }
                foreach (Thread th in threads)
                {
                    if (th != null && th.IsAlive)
                        th.Join();
                }
            }

            foreach(string str in descripciones)
            {
                sal += str;
            }
            CrearTxt(sal, "Latino_Descripciones");
            return true;
        }catch(Exception exc)
        {
            errores.Add(exc.Message + " |Main Function|");
            return false;
        }
    }





    //------------------------------------------Timer Function--------------------------------------------
    protected void Timer1_Tick(object sender, EventArgs e)
    {
        if (inProcess)
        {
            lbl1.Text = (DateTime.Now - time).ToString();
            TextBox1.Text = content;
        }
        if (processComplete && content.Contains(processCompleteMsg)) //has final message been set?
        {
            Timer1.Enabled = false;
            inProcess = false;
            Button1.Enabled = true;
            if (_pool != null)
                _pool.Dispose();
        }
        else if (errores.Count > 0)
        {
            Button1.Enabled = true;
            processComplete = false;
            if (_pool != null)
                _pool.Dispose();

        }
    }

    //--------------------Metodos asincronos----------------------------------------------

    private void getMC(CotizadorLatinoClient latino, LatinoWS.DatosRequeridos dat, string v)
    {
        _pool.WaitOne();
        try
        {
            LatinoWS.ListMarca marca = new ListMarca();
            dat.datosAuto.ClaveModelo = v;
            marca = latino.ObtenerMarcas(dat);

            if(marca.Marcas!=null)
            {
                foreach (var marcas in marca.Marcas)
                {
                    this.marcas.Add(new marcasLat(marcas.Clave, marcas.Descripcion,v));
                }


                content = "Terminando de obtener las marcas del año " + v + System.Environment.NewLine + content;

            }

        }
        catch (Exception exc)
        {
            errores.Add(exc.Message + " |GetMarcas|");
            content = "ah habido un error obteniendo las marcas del año " + v + System.Environment.NewLine + content;
        }
        finally
        {
            _pool.Release();
        }
    }


    private void getSM(CotizadorLatinoClient latino, DatosRequeridos dat, marcasLat marca)

    {
        _pool.WaitOne();
        try
        {
            LatinoWS.ListSubMarca submarca = new ListSubMarca();
            dat.datosAuto.ClaveModelo = marca.año;
            dat.datosAuto.ClaveMarca = marca.clave;
            submarca = latino.ObtenerSubMarcas(dat);

            if (submarca.SubMarcas != null)
            {
                foreach (var submarcas in submarca.SubMarcas)
                {
                    this.submarcas.Add(new SubMarca(marca.clave, submarcas.Clave, submarcas.Descripcion, marca.año));
                }
                content = "Terminando de obtener las submarcas de la marca "+marca.clave+" del año " + marca.año + System.Environment.NewLine + content;

            }

        }
        catch (Exception exc)
        {
            errores.Add(exc.Message + " |GetMarcas|");
            content = "ah habido un error obteniendo las submarcas de la marca " + marca.clave + "del año " + marca.año + System.Environment.NewLine + content;
        }
        finally
        {
            _pool.Release();
        }
    }

    private void getDS(CotizadorLatinoClient latino, DatosRequeridos dat, SubMarca subMarca)
    {
        try
        {
            _pool.WaitOne();

            dat.datosAuto.ClaveMarca = subMarca.clv_marca;
            dat.datosAuto.ClaveModelo = subMarca.año;
            dat.datosAuto.ClaveSubMarca = subMarca.clv_submarca;

            LatinoWS.ListVehiculo vehiculo = new ListVehiculo();
            vehiculo = latino.ObtenerDescripcionVehiculo(dat);

            if(vehiculo.Vehiculos!=null)
            {
                foreach (var vehiculos in vehiculo.Vehiculos)
                {
                    descripciones.Add(subMarca.clv_marca + "|" +subMarca.clv_submarca + "|" + vehiculos.Clave + "|" + vehiculos.Descripcion + "|" + subMarca.año + "<br>");
                }
                content = "terminando de obtener las descripciones de la marca " + subMarca.clv_marca + " de la submarca " + subMarca.descripcion + " del año " + subMarca.año + System.Environment.NewLine + content;
            }
        }
        catch (Exception exc )
        {
            errores.Add(exc.Message + " |GetMarcas|");
            content = "ah habido un error obteniendo las descripciones de la marca " + subMarca.clv_marca + " de la submarca " + subMarca.descripcion+ " del año " + subMarca.año + System.Environment.NewLine + content;
        }
        finally
        {
            _pool.Release();
        }

    }

    //-----------------------------------FUNCIONES DE UTILERIA-----------------------------

    public String CrearTxt(String salida, String name)
    {
        try
        {
            FileStream file = File.Create(@"C:\Users\ultron\Desktop\" + name + ".txt");
            Byte[] info = new UTF8Encoding(true).GetBytes(salida);
            file.Write(info, 0, info.Length);
            file.Close();
            return "ok";
        }
        catch (Exception exc)
        {
            return exc.Message;
        }
    }

    private String Almacenar()
    {
        try
        {
            //se crea la conexion a la base de datos , los parametros tienen que cambiarse
            using (var conn = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=LatinoCatalogos;user id = servici1; password = Master@011"))
            {
                //se accede y se ejecuta a el stop procedure y se le asigna un timeout de 0 para que no interrumpa la conexion
                using (var command = new SqlCommand("[SP_LatinoCatalogos]", conn) { CommandType = CommandType.StoredProcedure })
                {
                    command.CommandTimeout = 0;
                    conn.Open();
                    command.ExecuteNonQuery();
                    conn.Close();


                    return "ok";
                }
            }
        }
        catch (Exception excep)
        {
            return excep.Message;
        }
    }


}

public class marcasLat
{
    public string clave { set; get; }

    public string descripcion { set; get; }

    public string año { get; set; }

    public marcasLat(string clave, string descripcion, string año)
    {
        this.clave = clave;
        this.descripcion = descripcion;
        this.año = año;
    }
}

public class SubMarca
{
    public string clv_marca { set; get; }

    public string clv_submarca { set; get; }

    public string descripcion { set; get; }

    public string año { get; set; }

    public SubMarca(string clv_marca,string clv_submarca,string descripcion, string año)
    {
        this.clv_marca = clv_marca;
        this.clv_submarca = clv_submarca;
        this.descripcion = descripcion;
        this.año = año;
    }
}
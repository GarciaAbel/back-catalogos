﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" Async="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title></title>
</head>
<body>
<form id="form1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
<asp:Timer ID="mainTimer" runat="server" Interval="500" OnTick="mainTimer_Tick" Enabled="false">
</asp:Timer>
<table cellspacing="20" cellpadding="20" width="100%">
<tr>
<td align="center" valign="middle">
<div>
<asp:UpdatePanel ID="subPanel" runat="server" UpdateMode="Conditional">
<Triggers>
<asp:AsyncPostBackTrigger ControlID="mainTimer" />
</Triggers>
<ContentTemplate>
<asp:Image ID="imgProgress" runat="server" ImageUrl="~/Images/progress.gif" AlternateText=""
Visible="false" />
<br />
<asp:Label ID="lblDescription" runat="server" Visible="false"></asp:Label>
</ContentTemplate>
</asp:UpdatePanel>
</div>
</td>
</tr>
<tr>
<td align="center" valign="middle">
<asp:Button ID="btnLoop" runat="server" Text="Start Checking Status" OnClick="btnLoop_Click" style="height: 26px" />
</td>
</tr>
</table>
</form>
</body>
</html>
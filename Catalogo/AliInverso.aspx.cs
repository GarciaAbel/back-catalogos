﻿using HALIWS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Default2 : System.Web.UI.Page
{

    private static Boolean process = false;
    private static Boolean processComplet = false;
    private static String content = "";

    private delegate void SafeCallDelegate(string text);


    private string usr = "AhorraSeguros";
    private string pass = "Ah0rraS3guros2017";

    private List<String> errores;
    private List<marcaALI> marcas;
    private List<modelo> modelos;
    private List<Submarcas> sub;
    private List<DescripcionA> desc;
    private List<DescripcionB> descB;
    private String path = @"C:\Users\ultron\Desktop\";



    public string[] aseguradoras = { "MAPFRE" };//,"ELAGUILA", "QUALITAS", "BXMAS", "AIG", "ATLAS", "MAPFRE2", "INBURSA", "GNP", "ANA", "ABA", "SURA", "HDI", "BANORTE", "GENERALDESEGUROS", "ABA2", "HDI2", "miituo", "AXA", "AFIRME", "PRIMEROSEGUROS", "ZURICH", "AIG2", "ELPOTOSI", "HDI3" };
    private Semaphore semaphore;
    public string name;
    private static DateTime time;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Timer_Tick(object sender, EventArgs e)
    {
        if(process)
        {
            lbl1.Text = "Tiempo Transcurrido " + (DateTime.Now - time).ToString();
            Text1.Text = content;
        }

        if(processComplet && content.Contains("Terminado"))
        {
            Timer.Enabled = false;
            process = false;
            Boton1.Enabled = true;
        }

    }

    protected void Boton1_Click(object sender, EventArgs e)
    {
        descB = new List<DescripcionB>();
        desc = new List<DescripcionA>();
        sub = new List<Submarcas>();
        modelos = new List<modelo>();
        marcas = new List<marcaALI>();
        errores = new List<string>();
        Timer.Enabled = true;
        process = true;
        processComplet = false;
        name = aseguradoras[0];

        content = "";

        time = DateTime.Now;

        Boton1.Enabled = true;
        if (semaphore != null)
            semaphore.Dispose();

        Thread work = new Thread(() => doThings());
        work.Start();

    }

    private void doThings()
    {
       
        try
        {

            HALIWS.WebServiceSoapClient client = new HALIWS.WebServiceSoapClient();

            if (errores.Count == 0 && getMarcas(client))
            {
                if (errores.Count == 0 && getModelos(client))
                {
                    if (errores.Count == 0 && getSubmarcas(client))
                    {
                        if (errores.Count == 0 && getDescHom(client))
                        {
                            if (errores.Count == 0 && getDesc(client))
                            {

                            }
                        }
                    }
                }
            }


            processComplet = true;
            content = "Terminado" + System.Environment.NewLine + content + System.Environment.NewLine;
        }
        catch(Exception exc)
        {
            errores.Add("ah habido un error en la funcion doThings " + exc.Message);
            foreach (string err in errores)
                content = "ERROR:" + err + content + System.Environment.NewLine;
            content = "Terminado por errores" + content + System.Environment.NewLine;

            Thread.Sleep(20);

            process = false;
            processComplet = true;
        }
    }

    //----------------------------------------FUNCIONES SINCRONAS---------------------------------
    private bool getMarcas(HALIWS.WebServiceSoapClient catalogo)
    {
        try
        {
            string salida = "";

            foreach(string str in aseguradoras)
            {
                marcaALI marca = JsonConvert.DeserializeObject<marcaALI>(catalogo.GetMarcas(this.usr, this.pass, str));
                marca.Aseguradora = str;
                marcas.Add(marca);
                content = "Terminando de extraer las marcas de " + str + System.Environment.NewLine + content ;
                Thread.Sleep(2000);
            }

            foreach(marcaALI marca in marcas)
            {
                foreach(ListadoMarca str in marca.ListadoMarcas)
                    salida += marca.Aseguradora + "|" + str.Marca + "<br>";
            }
            CrearTxt(salida, name+"_A_Marcas");
            return true;
        }catch(Exception exc)
        {
            throw new ArgumentException("error al generar las marcas " + exc.Message);
        }

    }


    private bool getModelos(WebServiceSoapClient client)
    {
        try
        {
            Thread[] threads = new Thread[28];
            string salida = "";

            for (int i = 0; i < marcas.Count;)
            {
                for(int j=0; j<28;j++)
                {
                    if(i<marcas.Count)
                    {
                        threads[j] = new Thread(() => getMD(marcas[i] , client));
                        threads[j].Start();
                        Thread.Sleep(2000);
                    }
                    else
                        break;
                    
                    i++;
                }
                foreach (Thread th in threads)
                {
                    if (th != null)
                        th.Join();
                }
            }

            foreach (modelo mod in modelos)
            {
                foreach (ListadoDescripciones str in mod.ListadoDescripciones)
                    salida += mod.Aseguradora +"|" + mod.Marca + "|" + str.Modelo + "<br>";
            }
            CrearTxt(salida, name+"_A_modelos");

            return true;
        }
        catch(Exception exc)
        {
            throw new ArgumentException("error al generar los modelos " + exc.Message);
        }
    }


    private bool getSubmarcas(WebServiceSoapClient client)
    {
        try
        {
            string salida = "";
            semaphore = new Semaphore(1,100);
            Thread[] threads = new Thread[100];

            for(int i = 0; i<modelos.Count;)
            {
                for(int j = 0; j<100;j++)
                {
                    if (i < modelos.Count)
                    {
                        threads[j] = new Thread(() => getSB(modelos[i],client));
                        threads[j].Start();
                        Thread.Sleep(2000);
                    }
                    else
                        break;
                    i++;
                }
                foreach (Thread th in threads)
                {
                    if (th != null)
                        th.Join();
                }
            }

            foreach (Submarcas sub in this.sub)
            {
                foreach (ListadoDescripciones str in sub.ListadoDescripciones)
                    salida += sub.Aseguradora + "|" + sub.Marca + "|" + sub.Modelo + "|" + str.Descripcion + "<br>";
            }
            CrearTxt(salida, name+"_A_Submarcas");

            return true;
        }
        catch (Exception exc)
        {
            throw new ArgumentException("error al generar las submarcas " + exc.Message);
        }
    }

    private bool getDescHom(WebServiceSoapClient client)
    {
        try
        {
            string salida = "";
            semaphore = new Semaphore(1,100);
            Thread[] threads = new Thread[100];

            for(int i = 0; i<sub.Count;)
            {
                for(int j = 0; j < 100; j++)
                {
                    if (i < sub.Count)
                    {
                        threads[j] = new Thread(() => getDH(sub[i],client));
                        threads[j].Start();
                        Thread.Sleep(2000);
                    }
                    else
                        break;
                    i++;
                }
                foreach (Thread th in threads)
                {
                    if (th != null)
                        th.Join();
                }
            }
            foreach (DescripcionA sub in this.desc)
            {
                foreach (ListadoSubDescripciones str in sub.ListadoSubDescripciones)
                    salida += sub.Aseguradora + "|" + sub.Marca + "|" + sub.Modelo + "|" + sub.Descripcion +"|" + str.SubDescripcion +"<br>";
            }
            CrearTxt(salida, name+"_A_DescHom");
            return true;
        }catch(Exception exc)
        {
            throw new ArgumentException("error al generar las descripciones homologadas " + exc.Message);
        }
    }

    private bool getDesc(WebServiceSoapClient client)
    {
        try
        {
            string salida = "";
            semaphore = new Semaphore(1, 100);
            Thread[] threads = new Thread[100];

            for (int i = 0; i < desc.Count;)
            {
                for (int j = 0; j < 100; j++)
                {
                    if (i < desc.Count)
                    {
                        threads[j] = new Thread(() => getD(desc[i], client));
                        threads[j].Start();
                        Thread.Sleep(2000);
                    }
                    else
                        break;
                    i++;
                }
                foreach (Thread th in threads)
                {
                    if (th != null)
                        th.Join();
                }
            }
            foreach (DescripcionB sub in this.descB)
            {
                foreach (ListadoDetalles str in sub.ListadoDetalles)
                    salida += sub.Aseguradora + "|" + sub.Marca + "|" + sub.Modelo + "|" + sub.Descripcion + "|" + sub.SubDescripcion + "|" + str.Detalle +"<br>";
            }
            CrearTxt(salida,name+ "_A_DescReal");
            return true;
        }
        catch (Exception exc)
        {
            throw new ArgumentException("error al generar las descripciones homologadas " + exc.Message);
        }
    }



    //-----------------------------------------Funciones asincronas-----------------------------
    private void getMD(marcaALI marcas, WebServiceSoapClient client)
    {
        try
        {

            foreach(ListadoMarca listado in marcas.ListadoMarcas)
            {
                modelo mod = JsonConvert.DeserializeObject<modelo>(client.GetModelos(this.usr, this.pass, listado.Marca, marcas.Aseguradora));
                mod.Aseguradora = marcas.Aseguradora;

                modelos.Add(mod);
                Thread.Sleep(2000);
                content = "Terminando de extraer los modelos de " + listado.Marca + System.Environment.NewLine + content;
            }
        }
        catch(Exception exc)
        {
            throw new ArgumentException("error al extraer las submarcas " + exc.Message);

        }

    }

    private void getSB(modelo modelo, WebServiceSoapClient client)
    {
        try
        {
            semaphore.WaitOne();

            foreach(ListadoDescripciones desc in modelo.ListadoDescripciones)
            {
                Submarcas submarca = JsonConvert.DeserializeObject<Submarcas>(client.GetDescripcion(this.usr, this.pass, modelo.Marca, desc.Modelo, modelo.Aseguradora));
                submarca.Aseguradora = modelo.Aseguradora;

                sub.Add(submarca);

                Thread.Sleep(2000);
                content = "Terminando de extraer las submarcas de " + modelo.Marca + System.Environment.NewLine + content;
            }

        }
        catch (Exception exc)
        {
            throw new ArgumentException("error al extraer las submarcas " + exc.Message);
        }
        finally
        {
            content = "Terminando de extraer las submarcas de " + modelo.Marca + System.Environment.NewLine + content;
            semaphore.Release();
        }
    }
    private void getDH(Submarcas submarcas, WebServiceSoapClient client)
    {
        try
        {
            semaphore.WaitOne();
            foreach(ListadoDescripciones desc in submarcas.ListadoDescripciones)
            {
                DescripcionA descripcion = JsonConvert.DeserializeObject<DescripcionA>(client.GetSubDescripcion(this.usr, this.pass, submarcas.Marca, submarcas.Modelo, desc.Descripcion, submarcas.Aseguradora));
                descripcion.Aseguradora = submarcas.Aseguradora;

                this.desc.Add(descripcion);
                Thread.Sleep(1000);
            }
           
        }catch(Exception exc)
        {
            throw new ArgumentException("error al extraer las descripciones " + exc.Message);
        }
        finally
        {
            content = "Terminando de extraer las descripciones de " + submarcas.Marca+ System.Environment.NewLine + content;
            semaphore.Release();
        }
    }
    private void getD(DescripcionA descripcionA, WebServiceSoapClient client)
    {
        try
        {
            semaphore.WaitOne();
            foreach (ListadoSubDescripciones desc in descripcionA.ListadoSubDescripciones)
            {
                DescripcionB descripcion = JsonConvert.DeserializeObject<DescripcionB>(client.GetDetalle(this.usr, this.pass, descripcionA.Marca, descripcionA.Modelo, descripcionA.Descripcion, desc.SubDescripcion, descripcionA.Aseguradora));
                descripcion.Aseguradora = descripcionA.Aseguradora;

                this.descB.Add(descripcion);
                Thread.Sleep(1000);
            }

        }
        catch (Exception exc)
        {
            throw new ArgumentException("error al extraer las descripciones reales " + exc.Message);
        }
        finally
        {
            content = "Terminando de extraer las descripciones reales de " + descripcionA.Descripcion + System.Environment.NewLine + content;
            semaphore.Release();
        }
    }


    //--------------------------------------------------------------------------------------

    private void CrearTxt(String salida, String name)
    {
        //inicia el intento de crear el archivo
        try
        {
            //genera el archivo dentro del escritorio con el nombre y la cadena dada
            FileStream file = File.Create(this.path + name + ".txt");
            Byte[] info = new UTF8Encoding(true).GetBytes(salida);
            file.Write(info, 0, info.Length);
            file.Close();
        }
        catch (Exception excep)
        {
            this.errores.Add(excep.Message + "|" + excep.InnerException + "|" + excep.StackTrace);
        }
    }

}

//----------------------------------MARCAS----------------------------------------------
public partial class marcaALI
{
    [JsonProperty("ListadoMarcas")]
    public ListadoMarca[] ListadoMarcas { get; set; }

    public string Aseguradora { get; set; }
}

public partial class ListadoMarca
{
    [JsonProperty("Marca")]
    public string Marca { get; set; }
}

//------------------------------------MODELOS------------------------------------------
public class ListadoDescripciones
{
    public string Modelo { get; set; }

    public string Descripcion { get; set; }

}

public class modelo
{
    public string Marca { get; set; }
    public string Aseguradora { get; set; }

    public List<ListadoDescripciones> ListadoDescripciones { get; set; }
}

//------------------------------------SubMarca-------------------------------------------

public class Submarcas
{
    public string Marca { get; set; }
    public string Modelo { get; set; }
    public string Aseguradora { get; set; }

    public List<ListadoDescripciones> ListadoDescripciones { get; set; }
}

public class DescripcionA
{
    public string Marca { get; set; }
    public string Modelo { get; set; }
    public string Aseguradora { get; set; }
    public string Descripcion { get; set; }

    public List<ListadoSubDescripciones> ListadoSubDescripciones { get; set; }
}

public class ListadoSubDescripciones
{
    public string SubDescripcion { get; set; }
}

public class DescripcionB
{
    public string Marca { get; set; }
    public string Modelo { get; set; }
    public string Aseguradora { get; set; }
    public string Descripcion { get; set; }
    public string SubDescripcion { get; set; }


    public List<ListadoDetalles> ListadoDetalles { get; set; }
}

public class ListadoDetalles
{
    public string Detalle { get; set; }
}
﻿using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Default2 : System.Web.UI.Page
{

    private delegate void SafeCallDelegate(string text);
    private String path = @"C:\Users\ultron\Desktop\";
    private List<String> errores ;
    private List<string> track;
    protected static string content;
    protected static bool inProcess = false;
    protected static bool processComplete = false;
    protected static string processCompleteMsg = "Finished Processing All Records.";
    private static Semaphore _pool;


    private string usr = "773477";


    protected void Page_Load(object sender, EventArgs e)
    {

    }


    protected void Timer1_Tick(object sender, EventArgs e)
    {
        if (inProcess)
            TextBox1.Text = content;

        if (processComplete && content.Contains(processCompleteMsg)) //has final message been set?
        {
            Timer1.Enabled = false;
            inProcess = false;
            Button1.Enabled = true;
        }
    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        errores = new List<String>();
        track = new List<string>();


        Timer1.Enabled = true;
        Thread th = new Thread(() => doThings());
        th.Start();
    }

    public async void doThings()
    {
        try
        {
            inProcess = true;

            if(errores.Count==0&& await getMarcas())
            {

            }

            processComplete = true;


        }catch(Exception exc)
        {
            this.errores.Add(exc.Message + " |main| ");
        }

    }

    public async Task<Boolean> getMarcas()
    {
        try
        {

            Thread[] threads = new Thread[100];
             _pool = new Semaphore(0,20);

            for(int i = 0; i < 100; i++)
            {

                if (i+2000 < DateTime.Now.Year + 1)
                {
                    content = "Comenzando con las marcas del año " + (i + 2000).ToString() + System.Environment.NewLine + content;
                    threads[i] = new Thread(() => getMS(i));
                    threads[i].Start();
                    Thread.Sleep(30);
                    _pool.WaitOne();
                }
                else{
                    break;
                }
            }

            for(int j = 0; j < 100; j++ )
            {
                threads[j].Join();
            }
            _pool.Release(20);

            return true;
        }catch(Exception exc)
        {
            errores.Add(exc.Message + "|Api Consume|");
            return false;
        }
    }

    public void getMS(int año)
    {
        try
        {
            System.Diagnostics.Debug.WriteLine("Proceso numero " + (año).ToString());
            string json = "{\"usuario\": \"COTAHORRASEG\",\"tipoVehiculo\": \"AUT\",\"anio\": \"" + (año+2000).ToString() +"\"}";
            String sal = ApiConsume("https://api.elpotosi.com.mx/CotizadorAutos/v1/Catalogo/marcas" , json);
            if(sal!="error")
            {

            }

        }
        catch(Exception exc)
        {
            errores.Add(exc.Message + " getMS ");
        }
        content = "Completando con las marcas del año " + (año+2000).ToString() + System.Environment.NewLine + content;
        _pool.Release();

    }


    public String ApiConsume(String url, String Content)
    {
        try
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            //Creacion del objeto RestSharp
            var client = new RestClient(url);
            //Añadiendo las credenciales para la conexion para el objeto
            //client.Authenticator = new HttpBasicAuthenticator("COTAHORRASEG" , "");
            //Creacion de un request por metodo post
            var request = new RestRequest(Method.POST);
            //Declaracion de los headers del objeto declaracion 
            //Le decimos que acepte respuestas de todo tipo sin importar el formato 
            //request.AddHeader("Accept", "*/*");
            //Declaramos que el tipo de cuerpo va a ser en xml
            request.AddHeader("Content-Type", "application/json;");
            //Añadimos el cuerpo a el request
            request.AddParameter("application/json", Content, ParameterType.RequestBody);
            //Ejecutamos el request y cuardamos la respuesta en un objeto de tipo IRestResponse que es parte de RestSharp
            IRestResponse response = client.Execute(request);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                return response.Content + "error en la peticion <br>";
            }
            else
                return response.Content;
        }

        catch (Exception exc)
        {
            errores.Add(exc.Message + "|Api Consume|");
            return "error";
        }
    }




}
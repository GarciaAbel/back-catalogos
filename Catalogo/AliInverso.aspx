﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AliInverso.aspx.cs" Inherits="Default2" Async ="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="ScriptManager" runat="server"></asp:ScriptManager>
            <asp:Timer ID="Timer" runat="server" OnTick="Timer_Tick" Enabled ="false" Interval ="1"></asp:Timer>
           <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode ="Always">
               <Triggers>
                   <asp:AsyncPostBackTrigger ControlID="Timer" EventName="Tick" />
               </Triggers>
               <ContentTemplate>
                   <asp:TextBox runat="server" ID="Text1" Height ="250px" TextMode="MultiLine" Width="800px"></asp:TextBox>
                   <br />
                   <asp:Button ID="Boton1" runat="server" OnClick="Boton1_Click" Text="Actualizar"/>

                   <br />
                   <asp:Label runat="server" ID="lbl1"></asp:Label>
               </ContentTemplate>

           </asp:UpdatePanel>


        </div>
    </form>
</body>
</html>

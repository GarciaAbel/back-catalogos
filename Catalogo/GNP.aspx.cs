﻿using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;

public partial class _Default : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        String[] xmls = { "VEHICULOS", "ARMADORA_VEHICULO", "CARROCERIA_VEHICULO", "MODELO_VEHICULO", "TIPO_CARGA_VEHICULO", "TIPO_VEHICULO", "USO_VEHICULO", "VERSION_VEHICULO" };
        String[] xmlDesc = { "VehiculosGeneral", "Armadoras", "Carroceria", "Modelo", "TipoCarga", "TipoVehiculo", "UsoVehiculo", "VersionVehiculo" };
        String responseLoc = "<br>";
        for (int i = 0; i < 8; i++)
        {
            String mixml = "<SOLICITUD_CATALOGO><USUARIO>IPEREZ050031</USUARIO><PASSWORD>Junipg2017</PASSWORD><TIPO_CATALOGO>" + xmls[i] + "</TIPO_CATALOGO><ID_UNIDAD_OPERABLE/><FECHA>01/01/1800</FECHA></SOLICITUD_CATALOGO>";
            responseLoc += this.ApiConsume("https://api.service.gnp.com.mx/autos/wsp/catalogos/catalogo", mixml, xmlDesc[i]);
        }

        //se muestran las respuestas de la operacion en un label
        Label1.Text = responseLoc;
        Label2.Text =  StopProcedure();
    }

    public String ApiConsume(String url, String Content, String version)
    {
        //Creacion del objeto RestSharp
        var client = new RestClient(url);
        //Añadiendo las credenciales para la conexion para el objeto
        client.Authenticator = new HttpBasicAuthenticator("IPEREZ050031", "Junipg2017");
        //Creacion de un request por metodo post
        var request = new RestRequest(Method.POST);
        //Declaracion de los headers del objeto declaracion 
        //Le decimos que acepte respuestas de todo tipo sin importar el formato 
        request.AddHeader("Accept", "*/*");
        //Declaramos que el tipo de cuerpo va a ser en xml
        request.AddHeader("Content-Type", "application/xml;");
        //Añadimos el cuerpo a el request
        request.AddParameter("application/xml", Content, ParameterType.RequestBody);
        //Ejecutamos el request y cuardamos la respuesta en un objeto de tipo IRestResponse que es parte de RestSharp
        IRestResponse response = client.Execute(request);
        if (response.StatusCode != System.Net.HttpStatusCode.OK)
        {
            return response.Content + "error en la peticion de " + version + "<br>";
        }
        //Guardamos el resultado obtenido en un archivo de xml 
        try
        {
            FileStream file = File.Create(@"C:\Users\ultron\Desktop\" + version + ".xml");
            Byte[] info = new UTF8Encoding(true).GetBytes(response.Content);
            file.Write(info, 0, info.Length);
            file.Close();
        }
        catch (Exception exc)
        {
            return exc.Message + "Error al generar el archivo " + version + "<br>";
        }

        return version + " ok<br>";
    }

    //Se accede al storeprocedure de la base de datos
    public string StopProcedure()
    {
        try
        {
            //se crea la conexion a la base de datos , los parametros tienen que cambiarse
            using (var conn = new SqlConnection(@"Data Source=DESKTOP-D03IEPJ\SQLEXPRESS;Initial Catalog=GNPCATALOGOS;Integrated Security = True"))
            {
                //se accede y se ejecuta a el stop procedure y se le asigna un timeout de 0 para que no interrumpa la conexion
                using (var command = new SqlCommand("Sp_GNP_CATALOGO", conn) { CommandType = CommandType.StoredProcedure })
                {
                    command.CommandTimeout = 0;
                    conn.Open();
                    command.ExecuteNonQuery();
                    conn.Close();
                    return command.ToString();
                }
            }
        }
        catch (Exception excep)
        {
            return excep.Message;
        }
    }

}

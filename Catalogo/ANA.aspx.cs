﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class Default2 : System.Web.UI.Page
{

    private delegate void SafeCallDelegate(string text);
    private String path = @"C:\Users\ultron\Desktop\";
    private List<String> errores = new List<String>();
    private List<string> track = new List<string>();

    private List<MarcasANA> marcas = new List<MarcasANA>();

    private string usr = "6010";
    private string pass = "7tUdegub";


    private static Semaphore _pool;
    private static int _padding;


    protected static string content;
    protected static bool inProcess = false;
    protected static bool processComplete = false;
    protected static string processCompleteMsg = "Finished Processing All Records.";

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        Timer1.Enabled = true;
        Thread work = new Thread(new ThreadStart(doThings));
        work.Start();

    }

    public async void doThings()
    {

        try
        {
            
            inProcess = true;
            ANA.ServiceSoapClient ana = new ANA.ServiceSoapClient();


            if (await getMarcas(ana))
            {
                content = "Extraccion de marcas completada" + content + System.Environment.NewLine;
                if (await getModelos(ana))
                {
                    content = "Extraccion de Submarcas completada" + content + System.Environment.NewLine;
                }
            }
            processComplete = true;
            content = processCompleteMsg + System.Environment.NewLine + content;

        }
        catch (Exception exc)
        {
            errores.Add(exc.Message + " | getMarcas |<br>");
        }


    }


    public async Task<Boolean> getModelos(ANA.ServiceSoapClient ana)
    {
        try
        {
            int k = 0;
            Thread[] thread = new Thread[100];
            _pool = new Semaphore(0,50);
            for(int i = 0; i<marcas.Count;)
            {

                for (int j=0;j<100;j++)
                {
                    if ((i + j) < marcas.Count)
                    {
                        content = "Comenzando el proceso con la marca " + marcas[i+j].marca + " Del año " + marcas[i+j].año + System.Environment.NewLine + content;
                        thread[j] = new Thread(() => getSM(marcas[i+j], ana));
                        System.Diagnostics.Debug.WriteLine("Proceso numero " + (j+i).ToString());
                        thread[j].Start();
                        Thread.Sleep(100);
                        _pool.WaitOne();
                        k++;
                    }       
                }
                i += k;
                k = 0;

            }

            for (int i = 0; i < 100; i++)
                thread[i].Join();

            return true;
        }
        catch(Exception exc)
        {
            errores.Add(exc.Message + " | getMarcas |<br>");
            return false;
        }
    }

    public void getSM(MarcasANA marc, ANA.ServiceSoapClient an)
    {
        try
        {
            string salida = an.SubMarca(995,marc.marca,Int32.Parse(marc.año),100,this.usr,this.pass);

            if(salida != "<?xml version=\"1.0\" encoding=\"UTF-8\"?><transacciones xmlns=\"\"><error></error></transacciones>")
            {

            }


        }catch(Exception exc)
        {
            errores.Add(exc.Message + " | getMarcas |<br>");
        }

        content = "Completando el proceso con la marca " + marc.marca + " Del año " + marc.año + System.Environment.NewLine + content;
        _pool.Release();

    }


    protected void Timer1_Tick(object sender, EventArgs e)
    {
        if (inProcess)
            TextBox1.Text = content;

        if (processComplete && content.Contains(processCompleteMsg)) //has final message been set?
        {
            Timer1.Enabled = true;
            inProcess = false;
            Button1.Enabled = true;
        }
    }

    public async Task<Boolean> getMarcas(ANA.ServiceSoapClient ana)
    {
        string sal = "";
        try
        {

            Thread[] threads = new Thread[25];
            _pool = new Semaphore(0,10);

            for(int j = 0; j<25;j++)
            {
                content = "Comenzando el proceso , marcas del año " + (j + 1997).ToString() + "..." + System.Environment.NewLine + content;
                threads[j] = new Thread(() => getMS((j+ 1997), ana));
                threads[j].Start();
                Thread.Sleep(100);
                _pool.WaitOne();

            }
            for (int j = 0; j<25;j++)
            {
                threads[j].Join();
            }
            foreach (MarcasANA marcas in this.marcas)
            {
                sal += marcas.id + "|" + marcas.marca + "|" + marcas.año+"<br>"; 
            }



            CrearTxt(sal,"ANA_MARCAS");
            _pool.Release(10);

            return true;
        }
        catch (Exception exc)
        {
            errores.Add(exc.Message + " | getMarcas |<br>");
            return false;
        }
    }

    public void getMS(int i , ANA.ServiceSoapClient ana)
    {
        try
        {
            string sal = ana.Marca(995,i,100,this.usr,this.pass);
            if(sal!="<?xml version=\"1.0\" encoding=\"UTF-8\"?><transacciones xmlns=\"\"><error></error></transacciones>")
            {
                XDocument doc = XDocument.Parse(sal);
                var list = (from d in doc.Descendants("marca")
                            select new
                            {
                                id = d.Attribute("id").Value,
                                nombre = d.Value

                            }).ToList();
                            
                foreach(var lista in list)
                {
                    this.marcas.Add(new MarcasANA(lista.id, lista.nombre, i.ToString()));
                }
            }

        }catch(Exception exc)
        {
            errores.Add(exc.Message + " | getMarcas |<br>");
        }

        _pool.Release();
        content = "Completando el proceso , marcas del año " + (i).ToString() + "..." + System.Environment.NewLine + content;
    }




    private void CrearTxt(String salida, String name)
    {
        //inicia el intento de crear el archivo
        try
        {
            //genera el archivo dentro del escritorio con el nombre y la cadena dada
            FileStream file = File.Create(this.path + name + ".txt");
            Byte[] info = new UTF8Encoding(true).GetBytes(salida);
            file.Write(info, 0, info.Length);
            file.Close();
        }
        catch (Exception excep)
        {
            this.errores.Add(excep.Message + "|" + excep.InnerException + "|" + excep.StackTrace);
        }
    }



}



public class MarcasANA
{
    public string id { get; set; }
    public string marca { set; get; }

    public string año { get; set; }

    public MarcasANA(string id,string marcas,string año)
    {
        this.id = id;
        this.marca = marcas;
        this.año = año;
    }
}
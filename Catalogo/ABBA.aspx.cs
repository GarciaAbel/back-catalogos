﻿//Garcia Sanchez Alberto Abel
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Serialization;

public partial class _Default : System.Web.UI.Page
{
    private String usuario = "WSTRIGARANTE";
    private String pass = "VIRTUAL1$";
    private List<String> errores;
    private String path = @"C:\Users\ultron\Desktop\";
    private CAT marcas;
    private List<CAT> SUBMARCAS;
    private List<CAT> tipovehiculo;
    private String descripciones;
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        SUBMARCAS = new List<CAT>();
        tipovehiculo = new List<CAT>();
        this.errores = new List<String>();
        String solicitudes = "<CAT><NEG>7190</NEG>";
        String errores = "";
        ABAWS.ACCatalogosClient abba = new ABAWS.ACCatalogosClient();
        ABAWS.Token tok = new ABAWS.Token();
        tok.usuario = this.usuario;
        tok.password = this.pass;

        this.marcas = getMarcas(solicitudes, abba, tok);


        if (this.marcas != null)
        {
            getSubMarcas(solicitudes, abba, tok);

            if (SUBMARCAS.Count != 0)
            {
                getTipoVehiculo(solicitudes, abba, tok);
                if (tipovehiculo.Count != 0)
                {
                    getDescripcionVehiculo(solicitudes, abba, tok);
                    if (this.descripciones != "")
                    {
                        if (Almacenar() != "ok")
                            this.errores.Add("hubo un problema al almacenar las las tablas");
                    }
                    else
                        this.errores.Add("hubo un problema al generar las descripciones");
                }
                else
                    this.errores.Add("hubo un problema al generar los tipos de vehiculo");

            }
            else
                this.errores.Add("hubo un problema al generar las Submarcas");
        }
        else
            this.errores.Add("hubo un problema al generar las marcas");

        if (this.errores.Count != 0)
        {
            foreach (String str in this.errores)
            {
                errores += str + "<br>";
            }
            Label1.Text = errores;
        }
        else
            Label1.Text = "OK";

        this.marcas = null;
        this.SUBMARCAS = null;
        this.tipovehiculo = null;
        this.errores = null;


    }

    public CAT getMarcas(String entrada , ABAWS.ACCatalogosClient abba, ABAWS.Token tok)
    {
        String xmlS = "";
        CAT cat = null;
        String salida = "";
        String errores = "";
        try
        {
            xmlS = abba.ObtenerMarcas(tok, entrada+"</CAT>");
            cat = deserializarMarcas(xmlS);
            foreach(MARCA marc in cat.marcas)
            {
                salida += marc.DESC + "|" + marc.ID + "</br>";
            }

            CrearTxt(salida,"ABBA_MARCAS");
            return cat;
        }
        catch (FaultException<ABAWS.Error> ex)
        {
            this.errores.Add(String.Format("ocurrio un error en :<br> Mensaje:{0}<br> :: Origen{1}<br> Stack: {2}", ex.Detail.Mensaje, ex.Detail.Origen, ex.Detail.StackTrace));
            return cat;
        }
        catch (TimeoutException time)
        {
            this.errores.Add(time.Message + "|" + time.InnerException + "|" + time.StackTrace);
            return cat;
        }
        catch (Exception excep)
        {
            this.errores.Add(excep.Message + "|" + excep.InnerException + "|" + excep.StackTrace);
            return cat;
        }
    }

    public void getSubMarcas(String entrada, ABAWS.ACCatalogosClient abba, ABAWS.Token tok)
    {
        String salida = "";
        String XMLout = "";
        string errores = "";

        foreach (MARCA marcas in this.marcas.marcas)
        {
            try
            {
              XMLout = abba.ObtenerSubMarcas(tok, entrada + "<ID>" + marcas.ID + "</ID></CAT>");
                this.SUBMARCAS.Add(deserializarMarcas(XMLout));
            }
            catch (FaultException<ABAWS.Error> ex)
            {
                
            }
            catch (TimeoutException time)
            {
                errores = time.Message + "|" + time.InnerException + "|" + time.StackTrace;
                this.errores.Add(errores);
            }
            catch (Exception excep)
            {

            }
        }

        foreach(CAT cat in this.SUBMARCAS)
        {
            foreach(SUBMARCA sub in cat.submarcas)
            {
                salida += sub.ID+"|"+sub.DESC+"|"+cat.ID+ "</br>";
            }
        }
        CrearTxt(salida, "ABBA_SUBMARCAS");

    }

    public void getTipoVehiculo(String entrada, ABAWS.ACCatalogosClient abba, ABAWS.Token tok)
    {
        String XMLout = "";
        String errores = "";
        String salida = "";
        CAT aux = null;

        for (int i = 1880; i < DateTime.Now.Year +2; i++)
            {
                foreach (CAT cat in this.SUBMARCAS)
                {
                    foreach(SUBMARCA sub in cat.submarcas)
                    {
                    try
                        {
                            XMLout = abba.ObtenerTipoVehiculos(tok, entrada + "<MOD>" + i.ToString() + "</MOD><TAR>170</TAR><MARCA>"+cat.ID+"</MARCA><SUBMARCA>"+sub.ID+"</SUBMARCA></CAT>");
                            if (XMLout != "<?xml version=\"1.0\" encoding=\"utf-8\"?><CAT><NEG>7190</NEG><TIPOS /></CAT>")
                            {
                            aux = deserializarMarcas(XMLout);
                            agregarTipoV(i.ToString(), cat.ID, sub.ID, aux);
                            }
                        }
                        catch (FaultException<ABAWS.Error> ex)
                        {
                        }
                       catch(TimeoutException time)
                        {
                        errores = time.Message + "|" + time.InnerException + "|" + time.StackTrace;
                        this.errores.Add(errores);
                        }
                        catch (Exception excep)
                        {
                        }
                    }
                }
            }
        foreach(CAT cat in this.tipovehiculo)
        {
            foreach(TIPO tipo in cat.TIPOS)
            {
                salida += cat.ID + "|" + cat.SUBID + "|" + cat.ANIOS + "|" + tipo.ID + "|" + tipo.DESC + "</br>";
            }
        }
        CrearTxt(salida, "ABBA_Tipo_Vehiculos");
    }

    public Boolean agregarTipoV(String anios, int id,int subid , CAT aux)
    {
        try
        {
            CAT tipoV = new CAT();
            tipoV.ANIOS = anios;
            tipoV.ID = id;
            tipoV.SUBID = subid;
            tipoV.TIPOS = aux.TIPOS;
            this.tipovehiculo.Add(tipoV);
            return true;

        }
        catch (Exception exp)
        {
            this.errores.Add("|"+exp.Message + "in agregartipoV|");
            return false;
        }
    }

    public void getDescripcionVehiculo(String entrada, ABAWS.ACCatalogosClient abba, ABAWS.Token tok)
    {
        String XMLout = "";
        String errores = "";
        CAT aunx = null;

        foreach (CAT cat in this.tipovehiculo){
            foreach(TIPO tipo in cat.TIPOS){
                try{
                    XMLout = abba.ObtenerDescripcionVehiculos(tok, entrada + "<MOD>" + cat.ANIOS + "</MOD><TAR>170</TAR><TIPOVEH>" + tipo.ID + "</TIPOVEH></CAT>");
                    aunx = deserializarMarcas(XMLout);
                    foreach(DESCRIPCION desc in aunx.descripciones)
                    {
                        this.descripciones += cat.ID + "|" + cat.SUBID + "|" + cat.ANIOS + "|" + desc.ID + "|" + desc.CVEVEH + "|" + desc.DESC + "|" + desc.OCUPANTES + "</br>";
                    }

                }
                catch (FaultException<ABAWS.Error> ex)
                {
                   //
                }
                catch (TimeoutException time)
                {
                    errores = time.Message + "|" + time.InnerException + "|" + time.StackTrace;
                    this.errores.Add(errores);
                }
                catch (Exception excep)
                {
                   //
                }
            }
        }

        CrearTxt(this.descripciones, "ABBA_descripciones");

    }

    public void CrearTxt(String salida, String name)
    {
        //inicia el intento de crear el archivo
        try
        {
            //genera el archivo dentro del escritorio con el nombre y la cadena dada
            FileStream file = File.Create(this.path + name + ".txt");
            Byte[] info = new UTF8Encoding(true).GetBytes(salida);
            file.Write(info, 0, info.Length);
            file.Close();
        }
        catch(Exception excep)
        {
            this.errores.Add(excep.Message + "|" + excep.InnerException + "|" + excep.StackTrace);
        }
    }

    public CAT deserializarMarcas(String name)
    {
        CAT catalogo = null;
        XmlSerializer serial = new XmlSerializer(typeof(CAT));

        try
        {
            using (var reader = new StringReader(name))
            {
                catalogo = (CAT)serial.Deserialize(reader);
                return catalogo;
            }
        }
        catch(Exception exv)
        {
            this.errores.Add(exv.Message);
            return catalogo;
        }
    }

    private String Almacenar()
    {
        try
        {
            //se crea la conexion a la base de datos , los parametros tienen que cambiarse
            using (var conn = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=ABBACatalogos;user id = servici1; password = Master@011"))
            {
                //se accede y se ejecuta a el stop procedure y se le asigna un timeout de 0 para que no interrumpa la conexion
                using (var command = new SqlCommand("SP_AbaCatalogos", conn) { CommandType = CommandType.StoredProcedure })
                {
                    command.CommandTimeout = 0;
                    conn.Open();
                    command.ExecuteNonQuery();
                    conn.Close();


                    return "ok";
                }
            }
        }
        catch (Exception excep)
        {
            return excep.Message;
        }
    }

}
[Serializable()]
public class MARCA
{
    [System.Xml.Serialization.XmlElementAttribute("ID")]
    public int ID { get; set; }
    [System.Xml.Serialization.XmlElementAttribute("DESC")]
    public String DESC { get; set; }
}

public class SUBMARCA
{
    [System.Xml.Serialization.XmlElementAttribute("ID")]
    public int ID { get; set; }
    [System.Xml.Serialization.XmlElementAttribute("DESC")]
    public String DESC { get; set; }
}

public class TIPO
{
    [System.Xml.Serialization.XmlElementAttribute("ID")]
    public int ID { get; set; }
    [System.Xml.Serialization.XmlElementAttribute("DESC")]
    public String DESC { get; set; }
}

public class DESCRIPCION 
{
    [System.Xml.Serialization.XmlElementAttribute("ID")]
    public long ID { get; set; }

    [System.Xml.Serialization.XmlElementAttribute("CVEVEH")]
    public long CVEVEH { get; set; }

    [System.Xml.Serialization.XmlElementAttribute("DESC")]
    public String DESC { get; set; }

    [System.Xml.Serialization.XmlElementAttribute("OCUPANTES")]
    public int OCUPANTES { get; set; }
}

[Serializable()]
[System.Xml.Serialization.XmlRoot("CAT")]
public class CAT
{
    [XmlArray("MARCAS")]
    [XmlArrayItem("MARCA", typeof(MARCA))]
    public MARCA[] marcas { set; get; }
    public int ID { set; get; }

    [XmlArray("SUBMARCAS")]
    [XmlArrayItem("SUBMARCA", typeof(SUBMARCA))]
    public SUBMARCA[] submarcas { set; get; }

    [XmlArray("TIPOS")]
    [XmlArrayItem("TIPO", typeof(TIPO))]
    public TIPO[] TIPOS { set; get; }

    [XmlArray("DESCRIPCIONES")]
    [XmlArrayItem("DESCRIPCION", typeof(DESCRIPCION))]
    public DESCRIPCION[] descripciones { set; get; }

    public String ANIOS { get; set; }

    public int SUBID { get; set; }

    public void agregar(String anio)
    {
        this.ANIOS = anio;
    }
}


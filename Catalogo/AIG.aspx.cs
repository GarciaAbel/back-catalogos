﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

public partial class Default2 : System.Web.UI.Page
{

    private String path = @"C:\Users\ultron\Desktop\";
    private List<String> errores;
    private string usr = "ws.brandon.guadarrama";
    private string pass = "Webserv.aig10$";
    private List<MarcaAIG> marcas;
    private List<ModeloAIG> modelos;
    private List<string> track;


    protected void Page_Load(object sender, EventArgs e)
    {

    }


    public Boolean verificar(string name)
    {
        try
        {
            if (File.Exists(this.path + name + ".txt"))
            {

                track.Add(name);
                return true;
            }
            else
            {
                insertar(name);
                return false;
            }
        }
        catch (Exception exc)
        {
            errores.Add(exc.Message + " |Verificar|<br>");
            return false;
        }
    }
    //sal += marca.desc_marca + "|" + modelo.cod_modelo + "|" + modelo.descripcion + "|" + i.ToString() + "<br>";

    public Boolean insertar(string name)
    {
        if (name == "AIG_Modelos")
        {
            if (marcas.Count != 0)
                return false;

            string tex = File.ReadAllText(this.path + "AIG_Marcas" + ".txt").Replace("<br>", "|");
            string[] sal = tex.Split('|');

            for (int i = 0; i < sal.Length - 1;)
            {
                marcas.Add(new MarcaAIG(Int32.Parse(sal[i]), sal[i + 1]));
                i += 2;
            }

            return false;
        }

        // sal += marca.desc_marca + "|" + modelo.cod_modelo + "|" + modelo.descripcion + "|" + i.ToString() + "<br>";

        if (name == "AIG_SISA")
        {
            if (modelos.Count != 0)
                return false;

            string tex = File.ReadAllText(this.path + "AIG_Modelos" + ".txt").Replace("<br>", "|");
            string[] sal = tex.Split('|');

            for (int i = 0; i < sal.Length - 1;)
            {
                modelos.Add(new ModeloAIG(sal[i], sal[i + 1], Int32.Parse(sal[i + 2]), sal[i + 3], sal[i + 4]));
                i += 5;
            }

            return false;
        }

        return false;
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
   
        modelos = new List<ModeloAIG>();
        marcas= new List<MarcaAIG>();

        errores = new List<string>();
        track = new List<string>();
        dothings();
    }

    public void delete()
    {
        foreach (string str in track)
        {
            File.Delete(path + str + ".txt");
        }
    }

    public async void dothings()
    {
        string sal = "";
        string[] names = { "AIG_Marcas", "AIG_Modelos", "AIG_SISA" };

        try
        {
            AIG.WsCatalogoClient aig = new AIG.WsCatalogoClient();
            aig.ClientCredentials.UserName.UserName = this.usr;
            aig.ClientCredentials.UserName.Password = this.pass;


            if (errores.Count == 0 && (verificar(names[0]) || await getMarcas(aig)))
            {
                if (errores.Count == 0 && (verificar(names[1]) || await getModelos(aig)))
                {
                    if (errores.Count == 0 && (verificar(names[2]) || await getSISA(aig)))
                    {
                        if(track.Count==3)
                        {
                            delete();
                        }
                        else
                        {
                            Almacenar();
                        }
                    }
                }
            }

        }
        catch (Exception exc)
        {
            errores.Add(exc.Message + " | doThings |<br>");
        }

        if (errores.Count != 0)
        {
            foreach (string str in errores)
                sal += str;
        }
        else
        {
            sal = "se completo con exito";
        }

        Label2.Text = sal;

    }

    public async Task<Boolean> getSISA(AIG.WsCatalogoClient aig)
    {
        try
        {


            string salida = "";

            foreach (ModeloAIG modelo in modelos)
            {
                string SISAS = await aig.ObtenerSisaModeloAsync(Int32.Parse(modelo.cod_marca), modelo.cod_modelo, Int32.Parse(modelo.año));

                XDocument doc = XDocument.Parse(SISAS);

                var list = (from d in doc.Descendants("Table")
                            select new
                            {
                                sisa = d.Element("sisa").Value,
                                cod_marca = d.Element("cod_marca").Value,
                                cod_modelo = d.Element("cod_modelo").Value,
                                cod_moneda = d.Element("cod_moneda").Value,
                                aaaa_fabrica = d.Element("aaaa_fabrica").Value,
                                fecha_importe = d.Element("fec_importe").Value,
                                imp_valor = d.Element("imp_valor").Value,
                                descripcion = d.Element("descripcion_larga").Value
                            }
                            ).ToList();

                foreach (var lista in list)
                {
                    salida += lista.cod_marca + "|" + lista.cod_modelo + "|" + lista.sisa + "|" + lista.cod_moneda + "|" + lista.aaaa_fabrica + "|" + lista.fecha_importe + "|" + lista.imp_valor + "|" + lista.descripcion + "<br>";
                }

            }

            CrearTxt(salida, "AIG_SISA");
            Label1.Text = salida;

            return true;
        } catch (Exception exc)
        {
            errores.Add(exc.Message + " | getMarcas |<br>");
            return false;
        }
    }


    public async Task<Boolean> getMarcas(AIG.WsCatalogoClient aig)
    {
        try
        {
            string salida = "";
            string marcas = await aig.ObtenerCatalogoMarcasAsync(1);

            XDocument doc = XDocument.Parse(marcas);

            var list = (from d in doc.Descendants("Table")
                        select new
                        {
                            cod_marca = d.Element("cod_marca").Value,
                            descripcion = d.Element("txt_desc").Value
                        }
                        ).ToList();

            foreach (var valor in list)
            {
                this.marcas.Add(new MarcaAIG(Int32.Parse(valor.cod_marca), valor.descripcion));
                salida += valor.cod_marca + "|" + valor.descripcion + "<br>";
            }
            //Label2.Text = salida;
            CrearTxt(salida, "AIG_Marcas");
            return true;
        }
        catch (Exception exc)
        {
            errores.Add(exc.Message + " | getMarcas |<br>");
            return false;
        }

    }
    public async Task<Boolean> getModelos(AIG.WsCatalogoClient aig)
    {
        try
        {
            string sal = "";
            for (int i = 1965; i < DateTime.Now.Year + 2; i++)
            {
                foreach (MarcaAIG marca in marcas)
                {
                    string modelos = await aig.ObtenerAniosModeloAct1Async(marca.cod_marca, i.ToString());


                    if (modelos.Contains("<NewDataSet>"))
                    {
                        XDocument doc = XDocument.Parse(modelos);

                        var list = (from d in doc.Descendants("Table")
                                    select new
                                    {
                                        cod_modelo = d.Element("cod_modelo").Value,
                                        descripcion = d.Element("descripcion_larga").Value
                                    }
                                    ).ToList();

                        foreach (var modelo in list)
                        {
                            this.modelos.Add(new ModeloAIG(marca.cod_marca.ToString(), marca.desc_marca, Int32.Parse(modelo.cod_modelo), modelo.descripcion, i.ToString()));
                            sal += marca.cod_marca.ToString() + "|" + marca.desc_marca + "|" + modelo.cod_modelo + "|" + modelo.descripcion + "|" + i.ToString() + "<br>";
                            System.Diagnostics.Debug.WriteLine(marca.desc_marca + "|" + modelo.cod_modelo + "|" + modelo.descripcion + "|" + i.ToString() + "<br>");
                        }


                    }
                }
            }
            Label1.Text = sal;
            CrearTxt(sal, "AIG_Modelos");
            return true;
        }
        catch (Exception exc)
        {
            errores.Add(exc.Message + " | getModelos |<br>");
            return false;
        }
    }

    private void CrearTxt(String salida, String name)
    {
        //inicia el intento de crear el archivo
        try
        {
            //genera el archivo dentro del escritorio con el nombre y la cadena dada
            FileStream file = File.Create(this.path + name + ".txt");
            Byte[] info = new UTF8Encoding(true).GetBytes(salida);
            file.Write(info, 0, info.Length);
            file.Close();
        }
        catch (Exception excep)
        {
            this.errores.Add(excep.Message + "|" + excep.InnerException + "|" + excep.StackTrace);
        }
    }


    private Boolean Almacenar()
    {
        try
        {
            //se crea la conexion a la base de datos , los parametros tienen que cambiarse
            using (var conn = new SqlConnection(@"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=AIGCatalogos;user id = servici1; password = Master@011"))
            {
                //se accede y se ejecuta a el stop procedure y se le asigna un timeout de 0 para que no interrumpa la conexion
                using (var command = new SqlCommand("[SP.AIGCatalogos]", conn) { CommandType = CommandType.StoredProcedure })
                {
                    command.CommandTimeout = 0;
                    conn.Open();
                    command.ExecuteNonQuery();
                    conn.Close();


                    return true;
                }
            }
        }
        catch (Exception excep)
        {
            this.errores.Add(excep.Message + " |Almacenar| \n");
            return false;
        }
    }
}


    public class MarcaAIG
{
    public int cod_marca { get; set; }
    public string desc_marca { get; set; }


    public MarcaAIG(int cod, string desc)
    {
        this.cod_marca = cod;
        this.desc_marca = desc;
    }
}


public class ModeloAIG
{
    public string cod_marca { get; set; }
    public string desc_marca { get; set; }
    public int cod_modelo { get; set; }
    public string desc_modelo { get; set; }
    public string año { get; set; }

    public ModeloAIG(string cod_marca,string des_marca,int cod, string desc , string año)
    {
        this.cod_marca = cod_marca;
        this.desc_marca = des_marca;
        this.cod_modelo = cod;
        this.desc_modelo = desc;
        this.año = año;
    }
}



USE [GNPCATALOGOS]
GO
/****** Object:  StoredProcedure [dbo].[Sp_GNP_CATALOGO]    Script Date: 22/07/2019 03:15:05 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Sp_GNP_CATALOGO]
	
AS
BEGIN
	--Eliminacion de campos en la tabla para evitar duplicados 
	TRUNCATE TABLE GNP_CATALOGOS;
	TRUNCATE TABLE GNP_CARROCERIA;
	TRUNCATE TABLE GNP_ARMADORAS;
	TRUNCATE TABLE GNP_MODELO;
	TRUNCATE TABLE GNP_TIPO_CARGA;
	TRUNCATE TABLE GNP_TIPO_VEHICULO;
	TRUNCATE TABLE GNP_USO_VEHICULO;
	TRUNCATE TABLE GNP_VERSION_VEHICULO;


	--se crea una variable de tipo xml
     declare @xml xml;

	 --se le asigna a la variable el contenido del archivo xml como un xml para su interpretacion
	select @xml = BulkColumn from openrowset(BULK 'C:\Users\ultron\Desktop\VehiculosGeneral.xml' , SINGLE_BLOB) X;

	--se hace una insercion en los campos
	INSERT INTO GNP_CATALOGOS(TIPO_VEHICULO,ARMADORA,MODELO,CARROCERIA,VERSION_T,CLAVE_MARCA,ALTOVALOR)
	SELECT
	--se interpreta el archivo xml como nodos y se accede a los valores dentro de las etiquetas raiz para despues tomar los valores
	Cat.detalles.value('ELEMENTO[1]/VALOR[1]','NVARCHAR(MAX)'),
	Cat.detalles.value('ELEMENTO[2]/VALOR[1]','NVARCHAR(MAX)'),
	Cat.detalles.value('ELEMENTO[3]/VALOR[1]','NVARCHAR(MAX)'),
	Cat.detalles.value('ELEMENTO[4]/VALOR[1]','NVARCHAR(MAX)'),
	Cat.detalles.value('ELEMENTO[5]/VALOR[1]','NVARCHAR(MAX)'),
	Cat.detalles.value('ELEMENTO[6]/VALOR[1]','NVARCHAR(MAX)'),
	Cat.detalles.value('ELEMENTO[7]/VALOR[1]','NVARCHAR(MAX)')	
	from
	--se declara cual es la etiqueta raiz y como se va a interpretar
	@xml.nodes('//CATALOGO/ELEMENTOS') as Cat(detalles)


	select @xml = BulkColumn from openrowset(BULK 'C:\Users\ultron\Desktop\Armadoras.xml' , SINGLE_BLOB) X;
	INSERT INTO GNP_ARMADORAS(CLAVE,NOMBRE,VALOR)
	SELECT
	--se interpreta el archivo xml como nodos y se accede a los valores dentro de las etiquetas raiz para despues tomar los valores
	Cat.detalles.value('CLAVE[1]','NVARCHAR(MAX)'),
	Cat.detalles.value('NOMBRE[1]','NVARCHAR(MAX)'),
	Cat.detalles.value('VALOR[1]','NVARCHAR(MAX)')
	from
	--se declara cual es la etiqueta raiz y como se va a interpretar
	@xml.nodes('//CATALOGO/ELEMENTOS/ELEMENTO') as Cat(detalles)

	select @xml = BulkColumn from openrowset(BULK 'C:\Users\ultron\Desktop\Carroceria.xml' , SINGLE_BLOB) X;
	INSERT INTO GNP_CARROCERIA(CLAVE,NOMBRE,VALOR)
	SELECT
	--se interpreta el archivo xml como nodos y se accede a los valores dentro de las etiquetas raiz para despues tomar los valores
	Cat.detalles.value('CLAVE[1]','NVARCHAR(MAX)'),
	Cat.detalles.value('NOMBRE[1]','NVARCHAR(MAX)'),
	Cat.detalles.value('VALOR[1]','NVARCHAR(MAX)')
	from
	--se declara cual es la etiqueta raiz y como se va a interpretar
	@xml.nodes('//CATALOGO/ELEMENTOS/ELEMENTO') as Cat(detalles)
	

		select @xml = BulkColumn from openrowset(BULK 'C:\Users\ultron\Desktop\Modelo.xml' , SINGLE_BLOB) X;
	INSERT INTO GNP_MODELO(CLAVE,VALOR)
	SELECT
	--se interpreta el archivo xml como nodos y se accede a los valores dentro de las etiquetas raiz para despues tomar los valores
	Cat.detalles.value('CLAVE[1]','NVARCHAR(MAX)'),
	Cat.detalles.value('VALOR[1]','NVARCHAR(MAX)')
	from
	--se declara cual es la etiqueta raiz y como se va a interpretar
	@xml.nodes('//CATALOGO/ELEMENTOS/ELEMENTO') as Cat(detalles)


		select @xml = BulkColumn from openrowset(BULK 'C:\Users\ultron\Desktop\TipoCarga.xml' , SINGLE_BLOB) X;
	INSERT INTO GNP_TIPO_CARGA(CLAVE,NOMBRE,VALOR)
	SELECT
	--se interpreta el archivo xml como nodos y se accede a los valores dentro de las etiquetas raiz para despues tomar los valores
	Cat.detalles.value('CLAVE[1]','NVARCHAR(MAX)'),
	Cat.detalles.value('NOMBRE[1]','NVARCHAR(MAX)'),
	Cat.detalles.value('VALOR[1]','NVARCHAR(MAX)')
	from
	--se declara cual es la etiqueta raiz y como se va a interpretar
	@xml.nodes('//CATALOGO/ELEMENTOS/ELEMENTO') as Cat(detalles)
	
	select @xml = BulkColumn from openrowset(BULK 'C:\Users\ultron\Desktop\TipoVehiculo.xml' , SINGLE_BLOB) X;
	INSERT INTO GNP_TIPO_VEHICULO(CLAVE,NOMBRE,VALOR)
	SELECT
	--se interpreta el archivo xml como nodos y se accede a los valores dentro de las etiquetas raiz para despues tomar los valores
	Cat.detalles.value('CLAVE[1]','NVARCHAR(MAX)'),
	Cat.detalles.value('NOMBRE[1]','NVARCHAR(MAX)'),
	Cat.detalles.value('VALOR[1]','NVARCHAR(MAX)')
	from
	--se declara cual es la etiqueta raiz y como se va a interpretar
	@xml.nodes('//CATALOGO/ELEMENTOS/ELEMENTO') as Cat(detalles)
	
	select @xml = BulkColumn from openrowset(BULK 'C:\Users\ultron\Desktop\UsoVehiculo.xml' , SINGLE_BLOB) X;
	INSERT INTO GNP_USO_VEHICULO(CLAVE,NOMBRE,VALOR)
	SELECT
	--se interpreta el archivo xml como nodos y se accede a los valores dentro de las etiquetas raiz para despues tomar los valores
	Cat.detalles.value('CLAVE[1]','NVARCHAR(MAX)'),
	Cat.detalles.value('NOMBRE[1]','NVARCHAR(MAX)'),
	Cat.detalles.value('VALOR[1]','NVARCHAR(MAX)')
	from
	--se declara cual es la etiqueta raiz y como se va a interpretar
	@xml.nodes('//CATALOGO/ELEMENTOS/ELEMENTO') as Cat(detalles)

	select @xml = BulkColumn from openrowset(BULK 'C:\Users\ultron\Desktop\VersionVehiculo.xml' , SINGLE_BLOB) X;
	INSERT INTO GNP_VERSION_VEHICULO(CLAVE,NOMBRE,VALOR)
	SELECT
	--se interpreta el archivo xml como nodos y se accede a los valores dentro de las etiquetas raiz para despues tomar los valores
	Cat.detalles.value('CLAVE[1]','NVARCHAR(MAX)'),
	Cat.detalles.value('NOMBRE[1]','NVARCHAR(MAX)'),
	Cat.detalles.value('VALOR[1]','NVARCHAR(MAX)')
	from
	--se declara cual es la etiqueta raiz y como se va a interpretar
	@xml.nodes('//CATALOGO/ELEMENTOS/ELEMENTO') as Cat(detalles)
	END
	exec Sp_GNP_CATALOGO

	--TRUNCATE TABLE GNP_CATALOGOS;